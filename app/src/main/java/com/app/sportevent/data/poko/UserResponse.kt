package com.app.sportevent.data.poko

import com.google.gson.annotations.SerializedName

data class UserResponse(
    var status: Int,
    var comment: String,
    var user: User,
    @SerializedName("new_message")
    var newMessage: Int
)

data class User(
    var id: Int = 0,

    @SerializedName("name")
    var name: String  = "",

    var email: String = "",
    var lang: String = "",
    @SerializedName("country_id")
    var countryId: Int = 0,

    @SerializedName("is_command")
    var isCommand: Int = 0,

    @SerializedName("city_id")
    var cityId: Int = 0,

    var phone: String = "",

    var icon: String = "",

    @SerializedName("event_active")
    var eventActive: Int = 0
)