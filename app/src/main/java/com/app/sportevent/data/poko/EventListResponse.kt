package com.app.sportevent.data.poko

import com.google.gson.annotations.SerializedName

data class EventListResponse(
    var events: ArrayList<Event>,
    var status: Int
)

data class Event(
    var id: Int = -1,
    var created: Long? = null,
    var name: String? = null,
    var gender: Int? = null,
    var date_stop: Long? = null,
    var date_start: Long? = null,
    var description: String? = null,
    var participant_sum: Int? = null,
    var participant_ante: String? = null,
    var description_ante: String? = null,
    var prize_pool: String? = null,
    var published: Int? = null,
    var status: Int? = null,
    var participant_count: Int? = null,
    var age_min: Int? = null,
    var age_max: Int? = null,
    var icon: String? = null,

    @SerializedName("type_participant")
    var memberType: TypeParticipantModel? = null,

    @SerializedName("type_event")
    var eventType: TypeEventModel? = null,

    @SerializedName("sport_kind")
    var sportKind: SportKindModel? = null,
    var country: CountryModel? = null,
    var city: City? = null,
    var user: User = User()
)