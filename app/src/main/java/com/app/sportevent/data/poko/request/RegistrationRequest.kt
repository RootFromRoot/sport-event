package com.app.sportevent.data.poko.request

data class RegistrationRequest(
   var name: String,
   var email: String,
   var pass: String,
   var lang: String,
   var country_id: Int,
   var device_token: String,
   var device: Int// 0 - android
)