package com.app.sportevent.data.poko.request

data class LoginRequest(
    var email: String,
    var pass: String,
    var device_token: String,
    var device: Int// 0 - android
)