package com.app.sportevent.data.util

import android.content.Context
import com.app.sportevent.data.PREF_NAME_TOKEN
import com.app.sportevent.data.VALUE_NOT_FOUND

class PrefManager(context: Context) {

    private val sharedPref = context.getSharedPreferences("Pref", Context.MODE_PRIVATE)

    fun saveToken(value: String) {
        with(sharedPref.edit()) {
            putString(PREF_NAME_TOKEN, value)
            apply()
        }
    }

    fun saveStringToPref(name: String, value: String) {
        with(sharedPref.edit()) {
            putString(name, value)
            apply()
        }
    }

    fun getToken() = sharedPref.getString(PREF_NAME_TOKEN, VALUE_NOT_FOUND)!!
    fun getStringFromPref(name: String) = sharedPref.getString(name, VALUE_NOT_FOUND)!!

}