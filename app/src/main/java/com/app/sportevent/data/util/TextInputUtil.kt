package com.app.sportevent.data.util

import android.widget.TextView
import com.app.sportevent.data.isNumber
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

object TextInputUtil {
    fun setObservedEtEnteredLenghtToTV(et: TextInputEditText, tv: TextView){
        et.addTextChangedListener(object : AppTextWatcher(){
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                tv.text = "${s.toString().length}/50"
            }
        })
    }
    fun observedEtIsNumber(et: TextInputEditText, til: TextInputLayout){
        et.addTextChangedListener(object : AppTextWatcher(){
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.toString().isNumber()) til.error = "Должно быть числом"
                else til.error = ""
            }
        })
    }
}