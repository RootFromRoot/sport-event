package com.app.sportevent.data

import com.app.sportevent.data.poko.*
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import timber.log.Timber

@JvmSuppressWildcards
interface Api {
    @FormUrlEncoded
    @POST("api/common/getCityGeo/")
    fun getCityByGeo(@FieldMap args: HashMap<String, Any?>): Deferred<Response<CityResponse>>

    @FormUrlEncoded
    @POST("api/common/getCountry/")
    fun getCountriesList(@Field("lang") lang: String): Deferred<Response<CountriesResponse>>

    @FormUrlEncoded
    @POST("api/common/getCityAutocomplete/")
    fun getCityListByCountry(
        @Field("lang") lang: String,
        @Field("word") word: String,
        @Field("country_iso") isoList: ArrayList<String>
    ): Deferred<Response<CityListResponse>>

    @FormUrlEncoded
    @POST("api/common/registration/")
    fun registrate(@FieldMap args: HashMap<String, Any?>): Deferred<Response<SignUpResponse>>

    @FormUrlEncoded
    @POST("api/common/login/")
    fun login(@FieldMap args: HashMap<String, Any?>): Deferred<Response<SignUpResponse>>

    @FormUrlEncoded
    @POST("api/common/loginFirebase/")
    fun firebaseSignIn(@FieldMap args: HashMap<String, Any?>?): Deferred<Response<SignUpResponse>>

    @FormUrlEncoded
    @POST("api/common/loginVK/")
    fun vkSignIn(@FieldMap args: HashMap<String, Any?>?): Deferred<Response<SignUpResponse>>

    @FormUrlEncoded
    @POST("api/common/loginFB/")
    fun fbSignIn(@FieldMap args: HashMap<String, Any?>?): Deferred<Response<SignUpResponse>>

    @FormUrlEncoded
    @POST("api/common/recoveryPass/")
    fun restorePassword(@Field("email") email: String): Deferred<Response<SimpleResponse>>

    @FormUrlEncoded
    @POST("api/common/getCommonData/")
    fun getCommonData(
        @Header("Authorization") token: String,
        @Field("lang") lang: String
    ): Deferred<Response<DictionariesResponse>>

    @POST("api/user/getUser")
    fun getUserInfo(@Header("Authorization") token: String): Deferred<Response<UserResponse>>

    @FormUrlEncoded
    @POST("api/user/setUser")
    fun updateUserInfo(
        @Header("Authorization") token: String,
        @FieldMap args: HashMap<String, Any?>
    ): Deferred<Response<SimpleResponse>>

    @FormUrlEncoded
    @POST("api/user/createEvent/")
    fun createEvent(
        @Header("Authorization") token: String,
        @Field("name") name: String,
        @Field("sport_kind_id") sportKindId: Int,
        @Field("type_event_id") typeEventId: Int,
        @Field("gender") sex: Int,
        @Field("type_participant_id") memberType: Int,
        @Field("country_id") countyId: Int,
        @Field("city_id") cityId: Int,
        @Field("date_start") startDate: Long,
        @Field("date_stop") endDate: Long,
        @Field("description") description: String,
        @Field("participant_sum") memberCount: Int,
        @Field("participant_ante") memberPayment: String,
        @Field("description_ante") includedToPayment: String,
        @Field("prize_pool") prizePool: String,
        @Field("age_min") ageMin: Int,
        @Field("age_max") ageMax: Int,
        @Field("icon") image: String?
    ): Deferred<Response<EventResponse>>

    @FormUrlEncoded
    @POST("api/user/delEvent/")
    fun deleteEvent(
        @Header("Authorization") token: String, @Field("id") id: Int
    ): Deferred<Response<Status>>

    @FormUrlEncoded
    @POST("api/user/updateEvent/")
    fun updateEvent(
        @Header("Authorization") token: String,
        @Field("id") id: Int,
        @Field("name") name: String?,
        @Field("sport_kind_id") sportKindId: Int?,
        @Field("type_event_id") typeEventId: Int?,
        @Field("gender") sex: Int?,
        @Field("type_participant_id") memberType: Int?,
        @Field("country_id") countyId: Int?,
        @Field("city_id") cityId: Int?,
        @Field("date_start") startDate: Long?,
        @Field("date_stop") endDate: Long?,
        @Field("description") description: String?,
        @Field("participant_sum") memberCount: Int?,
        @Field("participant_ante") memberPayment: Int?,
        @Field("description_ante") includedToPayment: String?,
        @Field("prize_pool") prizePool: String?,
        @Field("age_min") ageMin: Int?,
        @Field("age_max") ageMax: Int?,
        @Field("icon") image: String?
    ): Deferred<Response<Status>>

    @FormUrlEncoded
    @POST("api/user/setFilter/")
    fun filer(
        @Header("Authorization") token: String,
        @FieldMap sportKindIdList: Map<String, List<Int>>,
        @FieldMap typeEventIdList: Map<String, List<Int>>,
        @FieldMap memberTypeIdList: Map<String, List<Int>>,
        @FieldMap ageMinMaxList: Map<String, List<Int>>,
        @FieldMap countryIdList: Map<String, List<Int>>,
        @FieldMap cityIdList: Map<String, List<Int>>,
        @FieldMap dateStartEndList: Map<String, List<Long>>
    ): Deferred<Response<Status>>

    @POST("api/user/getEvents/")
    fun getEventList(@Header("Authorization") token: String): Deferred<Response<EventListResponse>>

/*
       @FormUrlEncoded
       @POST("api/user/publicEvent/")
       fun publishEvent(
           @Header("Authorization") token: String,
           @Field("id") id: Int
       ): Deferred<Response<EventResponse>>*/

    companion object {
        private val retrofit: Retrofit = Retrofit.Builder()
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(
                        HttpLoggingInterceptor { message -> Timber.i(message) }
                            .apply { level = HttpLoggingInterceptor.Level.BODY }
                    )
                    .build()
            )
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()

        fun get(): Api {
            return retrofit.create(Api::class.java)
        }
    }
}