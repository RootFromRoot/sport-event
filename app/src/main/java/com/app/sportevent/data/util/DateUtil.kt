package com.app.sportevent.data.util

import java.util.*

object DateUtil {
    private val c = Calendar.getInstance()

    fun millisToYear(milliseconds: Long): Int {
        c.timeInMillis = milliseconds
        return c.get(Calendar.YEAR)
    }

    fun millisToFormattedMonth(milliseconds: Long): String {
        c.timeInMillis = milliseconds
        val month = c.get(Calendar.MONTH) + 1
        return if (month < 10) "0$month" else month.toString()
    }

    fun millisToFormattedDay(milliseconds: Long): String {
        c.timeInMillis = milliseconds
        val day = c.get(Calendar.DAY_OF_MONTH)
        return if (day < 10) "0$day" else day.toString()
    }

    fun millisToMonth(milliseconds: Long): Int {
        c.timeInMillis = milliseconds
        return c.get(Calendar.MONTH)
    }

    fun millisToDay(milliseconds: Long): Int {
        c.timeInMillis = milliseconds
        return c.get(Calendar.DAY_OF_MONTH)
    }

    fun millisecondsFromCalendarView(year: Int, month: Int, day: Int): Long {
        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.DAY_OF_MONTH, day)
        return cal.timeInMillis
    }
}