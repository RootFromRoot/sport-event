package com.app.sportevent.data

import android.content.Context
import android.net.Uri
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.app.sportevent.data.poko.SportKindModel

inline fun <reified T> toArray(list: List<*>): Array<T> {
    return (list as List<T>).toTypedArray()
}

fun String.isNumber() = this.matches("-?\\d+(\\.\\d+)?".toRegex())

fun Context.toast(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}

fun Fragment.toast(text: String) {
    Toast.makeText(this.context, text, Toast.LENGTH_SHORT).show()
}

fun Context.getCursor(contentUri: Uri) =
    this.contentResolver.query(contentUri, null, null, null, null)

fun Long.sizeInKBytes() =  this / 1024

fun Long.toSeconds() = this / 1000
