package com.app.sportevent.data.poko.request

data class VkAuthRequest(
    var vk_user_id: Int,
    var email: String,
    var country_id: Int,
    var device_token: String,
    var device: Int,
    var lang: String
)