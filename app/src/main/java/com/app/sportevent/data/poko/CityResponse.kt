package com.app.sportevent.data.poko

data class CityResponse(
    var city: City,
    var country: CountryModel,
    var status: Int,
    var comment: String
)

data class City(
    var id: Int,
    var country: String,
    var lat: Double,
    var lng: Double,
    var name: String,
    var isSelected: Boolean = false
)

data class CityListResponse(
    var cities: ArrayList<City>,
    var status: Int
)