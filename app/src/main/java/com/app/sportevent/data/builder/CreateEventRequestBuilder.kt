package com.app.sportevent.data.builder

import com.app.sportevent.data.poko.request.CreateEventRequest

object CreateEventRequestBuilder {
    fun createEmpty() = CreateEventRequest(
        name = "",
        sport_kind_id = 0,
        type_event_id = 0,
        gender = 0,
        type_participant_id = 0,
        country_id = 0,
        city_id = 0,
        date_start = 0L,
        date_stop = 0L,
        description = "",
        participant_sum = 0,
        participant_ante = "",
        description_ante = "",
        prize_pool = "",
        age_min = 0,
        age_max = 0
    )
}