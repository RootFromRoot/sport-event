package com.app.sportevent.data

import com.app.sportevent.data.util.PrefManager
import com.app.sportevent.ui.auth.login.LoginModel
import com.app.sportevent.ui.auth.login.LoginPresenter
import com.app.sportevent.ui.auth.registration.RegistrationModel
import com.app.sportevent.ui.auth.registration.RegistrationPresenter
import com.app.sportevent.ui.event.create.CreateEventModel
import com.app.sportevent.ui.event.edit.EditEventModel
import com.app.sportevent.ui.event.info.EventModel
import com.app.sportevent.ui.filter.FilterModel
import com.app.sportevent.ui.list.EventListModel
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val applicationModule = module(override = true) {

    single { PrefManager(androidContext()) }

    single { RegistrationModel(get()) }
    single { RegistrationPresenter(get()) }

    single { LoginModel(get()) }
    single { LoginPresenter(get()) }

    single { CreateEventModel(androidContext()) }
    single { EventListModel(get()) }
    single { FilterModel(get()) }
    single { EventModel(androidContext()) }
    single { EditEventModel(get()) }
}