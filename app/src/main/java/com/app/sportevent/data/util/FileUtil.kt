package com.app.sportevent.data.util

import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Base64
import com.app.sportevent.data.getCursor
import com.app.sportevent.data.poko.GalleryImage
import com.app.sportevent.data.sizeInKBytes
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.IOException

object FileUtil {
    fun getImageFromUri(context: Context, contentUri: Uri): GalleryImage {
        val result = GalleryImage(contentUri.path!!, 0)
        val cursor = context.getCursor(contentUri)
        if (cursor != null) {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            val sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE)
            result.path = cursor.getString(idx)
            result.size = cursor.getLong(sizeIndex)
            cursor.close()
        }
        return result
    }

    fun isSizeValid(image: GalleryImage) = image.size != 0L && image.size.sizeInKBytes() < 200

    fun convertImageToBase64(imagePath: String?): String? {
        if (imagePath != null) {
            val inputStream = FileInputStream(File(imagePath))

            val buffer = ByteArray(8192)
            var bytesRead: Int
            val output = ByteArrayOutputStream()
            try {
                while (inputStream.read(buffer).also { bytesRead = it } != -1) {
                    output.write(buffer, 0, bytesRead)
                }
            } catch (e: IOException) {
                Timber.e(e, "Error during reading file")
                e.printStackTrace()
            }

            return "data:image/jpeg;base64," +
                    Base64.encodeToString(output.toByteArray(), Base64.DEFAULT)
        } else return null
    }
}