package com.app.sportevent.data.util

import android.content.Context
import android.location.Location
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import timber.log.Timber

object LocationManager {
    fun getLastLocation(context: Context, onFetched: (location: Location) -> Unit) {
        LocationServices
            .getFusedLocationProviderClient(context)
            .lastLocation
            .addOnSuccessListener {
                if (it != null) {
                    Timber.i("Location is ${it.latitude}")
                    onFetched(it)
                } else {
                    Timber.e("Location is null")
                    updateLocation { updatedLocation ->
                        Timber.e("Location updated: ${updatedLocation.latitude}")
                        onFetched(updatedLocation)
                    }
                }
            }
    }

    private fun updateLocation(onUpdate: (location: Location) -> Unit) {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 20 * 1000
        val locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult != null)
                    for (location in locationResult.locations) {
                        if (location != null) onUpdate(location)
                    }
            }
        }
    }
}