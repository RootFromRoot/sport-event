package com.app.sportevent.data.poko

data class GalleryImage(var path: String, var size: Long)