package com.app.sportevent.data.util.builder

import com.app.sportevent.data.poko.request.RegistrationRequest

object RegistrationRequestBuilder {
    fun buildeEmpty() = RegistrationRequest(
        name = "",
        email = "",
        pass = "",
        lang = "",
        country_id = 0,
        device_token = "",
        device = 0
    )
}