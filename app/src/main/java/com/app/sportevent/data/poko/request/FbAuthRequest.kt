package com.app.sportevent.data.poko.request

data class FbAuthRequest(
    var fb_token: String,
    var country_id: Int,
    var device_token: String,
    var device: Int,
    var lang: String
)