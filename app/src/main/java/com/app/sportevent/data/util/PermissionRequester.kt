package com.app.sportevent.data.util

import android.app.Activity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.app.sportevent.data.RC_PERMISSION_CAMERA
import com.app.sportevent.data.RC_PERMISSION_LOCATION
import com.app.sportevent.data.RC_PERMISSION_STORAGE

object PermissionRequester {
    fun requestLocationPermission(activity: Activity) {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ),
            RC_PERMISSION_LOCATION
        )
    }

    fun requestStoragePermission(activity: Activity) {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
            RC_PERMISSION_STORAGE
        )
    }

    fun requestCameraPermission(activity: Activity) {
        ActivityCompat.requestPermissions(
            activity, arrayOf(android.Manifest.permission.CAMERA), RC_PERMISSION_CAMERA
        )
    }

    fun requestStoragePermission(fr: Fragment) {
        fr.requestPermissions(
            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
            RC_PERMISSION_STORAGE
        )
    }


    fun requestLocationPermission(activity: Activity, rc: Int) {
        ActivityCompat.requestPermissions(
            activity, arrayOf(
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ), rc
        )
    }

    fun requestStoragePermission(fr: Fragment, rc: Int) {
        fr.requestPermissions(arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), rc)
    }

    fun requestPhonePermission(activity: Activity, rc: Int) {
        ActivityCompat.requestPermissions(
            activity, arrayOf(android.Manifest.permission.CALL_PHONE), rc
        )
    }

    fun requestLocationPermission(fr: Fragment, rc: Int) {
        fr.requestPermissions(
            arrayOf(
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ), rc
        )
    }
}