package com.app.sportevent.data.util

import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId

object FirebaseProvider {
    fun provideFireBaseToken(isFetched: (token: String) -> Unit) {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                isFetched(task.result!!.token)
            })
    }
}