package com.app.sportevent.data.poko

data class SignUpResponse(
    var token: String,
    var status: Int,
    var comment: String?
)