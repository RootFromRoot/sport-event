package com.app.sportevent.data.poko.request

data class LocationRequest(
    var lat: Double,
    val lng: Double,
    var lang: String
)