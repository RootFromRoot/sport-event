package com.app.sportevent.data.poko

data class SimpleResponse(
    var status: Int,
    var comment: String
)