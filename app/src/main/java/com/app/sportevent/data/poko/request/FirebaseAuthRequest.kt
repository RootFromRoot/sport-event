package com.app.sportevent.data.poko.request

data class FirebaseAuthRequest(
    var firebase_user_id: String,
    var country_id: Int,
    var device_token: String,
    var device: Int,
    var lang: String
)