package com.app.sportevent.data.builder

import com.app.sportevent.data.poko.DictionariesResponse

object DictionariesBuilder {
    fun empty() = DictionariesResponse(
        sportKind = ArrayList(),
        typeParticipant = ArrayList(),
        typeEvent = ArrayList(),
        country = ArrayList(),
        status = 0,
        comment = ""
    )
}