package com.app.sportevent.data.poko

data class EventResponse(
    var event: NewEvent,
    var status: Int
)

data class NewEvent(
    var sport_kind_id: Int,
    var type_event_id: Int,
    var gender: Int,
    var published: Int,
    var date_start: Long,
    var date_stop: Long,
    var type_participant_id: Int,
    var participant_sum: Int,
    var status: Int,
    var name: String,
    var dir_photo: String,
    var participant_ante: String,
    var description_ante: String,
    var prize_pool: String,
    var country_id: Int,
    var city_id: Int,
    var description: String,
    var created: Long,
    var user_id: String,
    var participant_count: String,
    var age_min: String,
    var age_max: String,
    var id: Int,
    var icon: String
)