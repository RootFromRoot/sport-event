package com.app.sportevent.data.poko.request

data class CreateEventRequest(
    var name: String,
    var sport_kind_id: Int,
    var type_event_id: Int,
    var gender: Int,// 0-смешаный,1-м, 2-ж
    var type_participant_id: Int,
    var country_id: Int,
    var city_id: Int,
    var date_start: Long,
    var date_stop: Long,
    var description: String,
    var participant_sum: Int,
    var participant_ante: String,
    var description_ante: String,
    var prize_pool: String,
    var age_min: Int,
    var age_max: Int
)