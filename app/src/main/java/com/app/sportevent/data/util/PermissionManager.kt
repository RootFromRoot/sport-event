package com.app.sportevent.data.util

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

object PermissionManager {

    fun checkIsPhonePermissionGranted(context: Context) =
        (ContextCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE)
                == PackageManager.PERMISSION_GRANTED)

    fun checkIsCameraPermissionGranted(context: Context) =
        (ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED)

    fun checkIsStoragePermissionGranted(context: Context) = (ContextCompat.checkSelfPermission(
        context, android.Manifest.permission.READ_EXTERNAL_STORAGE
    ) == PackageManager.PERMISSION_GRANTED)

    fun checkIsLocationPermissionGranted(context: Context) = (ContextCompat.checkSelfPermission(
        context, android.Manifest.permission.ACCESS_COARSE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED)

}