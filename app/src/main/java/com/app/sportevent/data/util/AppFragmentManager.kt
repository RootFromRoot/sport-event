package com.app.sportevent.data.util

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.app.sportevent.R


object AppFragmentManager {
    fun closeFragment(activity: FragmentActivity) {
        activity.supportFragmentManager.popBackStack()
    }

    fun openFragment(activity: FragmentActivity, fragment: Fragment) {
        activity.supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment, fragment.javaClass.name)
            .addToBackStack(fragment.javaClass.name)
            .commit()
    }

    fun openFragmentWithoutBackStark(activity: FragmentActivity, fragment: Fragment) {
        activity.supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment, fragment.javaClass.name)
            .commit()
    }


    fun clearBackStack(activity: FragmentActivity) {
        activity
            .supportFragmentManager
            .popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    /*fun changeFragment(activity: FragmentActivity, fragment: Fragment) {
        activity.apply {
            val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
            val currentFragment = supportFragmentManager.primaryNavigationFragment
            if (currentFragment != null) fragmentTransaction.hide(currentFragment)

            var fragmentTemp = supportFragmentManager.findFragmentByTag(fragment.javaClass.name)
            when (fragmentTemp) {
                null -> {
                    fragmentTemp = fragment
                    fragmentTransaction.add(R.id.container, fragmentTemp, fragment.javaClass.name)
                }
                else -> fragmentTransaction.show(fragmentTemp)
            }
                fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp)
                        fragmentTransaction . setReorderingAllowed (true)
                        fragmentTransaction . commitNowAllowingStateLoss ()
        }
    }*/

    fun replaceFragment(activity: FragmentActivity, fragment: Fragment) {
        val backStateName = fragment.javaClass.name
        activity.apply {
            val fragmentPopped = supportFragmentManager.popBackStackImmediate(backStateName, 0)
            if (!fragmentPopped) { //fragment not in back stack, create it.
                val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
                ft.replace(R.id.container, fragment)
                ft.addToBackStack(backStateName)
                ft.commit()
            }
        }
    }
}