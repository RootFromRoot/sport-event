package com.app.sportevent.data.poko

import com.google.gson.annotations.SerializedName

data class CountriesResponse(
    @SerializedName("country")
    var countries: ArrayList<CountryModel>,
    var status: Int,
    var comment: String
)