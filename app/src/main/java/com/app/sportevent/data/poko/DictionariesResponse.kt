package com.app.sportevent.data.poko

import com.google.gson.annotations.SerializedName

data class DictionariesResponse(
    @SerializedName("sport_kind")
    var sportKind: ArrayList<SportKindModel>,

    @SerializedName("type_participant")
    var typeParticipant: ArrayList<TypeParticipantModel>,

    @SerializedName("type_event")
    var typeEvent: ArrayList<TypeEventModel>,
    var country: ArrayList<CountryModel>,
    var status: Int,
    var comment: String
)

data class TypeEventModel(
    var id: Int,
    var name: String,
    var isSelected: Boolean = false
)

data class CountryModel(
    var id: Int,
    var iso: String,
    var name: String,
    var isSelected: Boolean = false
)

data class TypeParticipantModel(
    var id: Int,
    var age_min: Int,
    var age_max: Int,
    var name: String,
    var isSelected: Boolean = false
)

data class SportKindModel(
    var id: Int = 0,
    var icon: String = "",
    var name: String = "",
    var isSelected: Boolean = false
)