package com.app.sportevent.data.util

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.widget.Button
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.app.sportevent.R
import com.app.sportevent.data.poko.*
import com.app.sportevent.ui.base.BaseFragment
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar


object DialogBuilder {
    fun showSexDialog(
        ctx: Context,
        sexList: ArrayList<Sex>,
        onPositiveClick: (list: ArrayList<Sex>) -> Unit
    ) {
        val items: Array<String> = sexList.map { it.title }.toTypedArray()
        val checkedItems = BooleanArray(sexList.size)

        sexList.forEachIndexed { index, sex ->
            items[index] = sex.title
            checkedItems[index] = sex.isChecked
        }

        val builder = AlertDialog.Builder(ctx)
        builder.setMultiChoiceItems(items, checkedItems) { dialog, indexSelected, isChecked ->
            checkedItems[indexSelected] = isChecked
            sexList[indexSelected].isChecked = isChecked
        }.setPositiveButton(android.R.string.ok) { dialog, id ->
            onPositiveClick(sexList)
            dialog.dismiss()
        }.setNegativeButton(android.R.string.cancel) { dialog, id ->
            dialog.dismiss()
        }.show()
    }

    /*fun showMemberTypeDialog(
        ctx: Context,
        sexList: ArrayList<TypeParticipantModel>,
        onPositiveClick: (list: ArrayList<TypeParticipantModel>) -> Unit
    ) {
        val items: Array<String> = sexList.map { it.name }.toTypedArray()
        val checkedItems = BooleanArray(sexList.size)

        sexList.forEachIndexed { index, sex ->
            items[index] = sex.name
            checkedItems[index] = sex.isSelected
        }

        val builder = AlertDialog.Builder(ctx)
        builder.setMultiChoiceItems(items, checkedItems) { dialog, indexSelected, isChecked ->
            checkedItems[indexSelected] = isChecked
            sexList[indexSelected].isSelected = isChecked
        }.setPositiveButton(android.R.string.ok) { dialog, id ->
            onPositiveClick(sexList)
            dialog.dismiss()
        }.setNegativeButton(android.R.string.cancel) { dialog, id ->
            dialog.dismiss()
        }.show()
    }*/

    /*  fun showEventTypeDialog(
          ctx: Context,
          sexList: ArrayList<TypeEventModel>,
          onPositiveClick: (list: ArrayList<TypeEventModel>) -> Unit
      ) {
          val items: Array<String> = sexList.map { it.name }.toTypedArray()
          val checkedItems = BooleanArray(sexList.size)

          sexList.forEachIndexed { index, sex ->
              items[index] = sex.name
              checkedItems[index] = sex.isSelected
          }

          val builder = AlertDialog.Builder(ctx)
          builder.setMultiChoiceItems(items, checkedItems) { dialog, indexSelected, isChecked ->
              checkedItems[indexSelected] = isChecked
              sexList[indexSelected].isSelected = isChecked
          }.setPositiveButton(android.R.string.ok) { dialog, id ->
              onPositiveClick(sexList)
              dialog.dismiss()
          }.setNegativeButton(android.R.string.cancel) { dialog, id ->
              dialog.dismiss()
          }.show()
      }*/

    fun showMemberTypeDialog(
        ctx: Context,
        list: ArrayList<TypeParticipantModel>,
        onPositiveClick: (kind: TypeParticipantModel) -> Unit
    ) {
        var checkedItemIndex = 0
        list.forEachIndexed { index, sportKindModel ->
            if (sportKindModel.isSelected) checkedItemIndex = index
        }
        val items: Array<String> = list.map { it.name }.toTypedArray()
        val checkedItems = BooleanArray(list.size)

        list.forEachIndexed { index, sex ->
            items[index] = sex.name
            checkedItems[index] = sex.isSelected
        }

        val builder = AlertDialog.Builder(ctx)
        builder.setSingleChoiceItems(items, checkedItemIndex, null)
            .setPositiveButton(android.R.string.ok) { dialog, id ->
                val selectedPosition = (dialog as AlertDialog).listView.checkedItemPosition
                onPositiveClick(list[selectedPosition])
                dialog.dismiss()
            }.setNegativeButton(android.R.string.cancel) { dialog, id ->
                dialog.dismiss()
            }.show()
    }

    fun showEventTypeDialog(
        ctx: Context,
        list: ArrayList<TypeEventModel>,
        onPositiveClick: (kind: TypeEventModel) -> Unit
    ) {
        var checkedItemIndex = 0
        list.forEachIndexed { index, sportKindModel ->
            if (sportKindModel.isSelected) checkedItemIndex = index
        }
        val items: Array<String> = list.map { it.name }.toTypedArray()
        val checkedItems = BooleanArray(list.size)

        list.forEachIndexed { index, sex ->
            items[index] = sex.name
            checkedItems[index] = sex.isSelected
        }

        val builder = AlertDialog.Builder(ctx)
        builder.setSingleChoiceItems(items, checkedItemIndex, null)
            .setPositiveButton(android.R.string.ok) { dialog, id ->
                val selectedPosition = (dialog as AlertDialog).listView.checkedItemPosition
                onPositiveClick(list[selectedPosition])
                dialog.dismiss()
            }.setNegativeButton(android.R.string.cancel) { dialog, id ->
                dialog.dismiss()
            }.show()
    }

    fun showSportKindDialog(
        ctx: Context,
        list: ArrayList<SportKindModel>,
        onPositiveClick: (kind: SportKindModel) -> Unit
    ) {
        var checkedItemIndex = 0
        list.forEachIndexed { index, sportKindModel ->
            if (sportKindModel.isSelected) checkedItemIndex = index
        }
        val items: Array<String> = list.map { it.name }.toTypedArray()
        val checkedItems = BooleanArray(list.size)

        list.forEachIndexed { index, sex ->
            items[index] = sex.name
            checkedItems[index] = sex.isSelected
        }

        val builder = AlertDialog.Builder(ctx)
        builder.setSingleChoiceItems(items, checkedItemIndex, null)
            .setPositiveButton(android.R.string.ok) { dialog, id ->
                val selectedPosition = (dialog as AlertDialog).listView.checkedItemPosition
                onPositiveClick(list[selectedPosition])
                dialog.dismiss()
            }.setNegativeButton(android.R.string.cancel) { dialog, id ->
                dialog.dismiss()
            }.show()
    }

    fun showAgeDialog(
        ctx: Context,
        memberType: TypeParticipantModel,
        onPositiveClick: (min: Int, max: Int) -> Unit
    ) {
        val dialog = Dialog(ctx)
        dialog.setContentView(R.layout.dialog_age_filter)
        val seekBar = dialog.findViewById<CrystalRangeSeekbar>(R.id.seekbar_age)
        val btnOk = dialog.findViewById<Button>(R.id.btn_ok)
        val btnCancel = dialog.findViewById<Button>(R.id.btn_cancel)
        val tvStartValue = dialog.findViewById<TextView>(R.id.tv_start_value)
        val tvEndValue = dialog.findViewById<TextView>(R.id.tv_end_value)

        seekBar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            tvStartValue.text = minValue.toString()
            tvEndValue.text = maxValue.toString()
        }
        btnCancel.setOnClickListener { dialog.dismiss() }
        btnOk.setOnClickListener {
            onPositiveClick(seekBar.selectedMinValue.toInt(), seekBar.selectedMaxValue.toInt())
            dialog.dismiss()
        }

        seekBar.setMinValue(memberType.age_min.toFloat())
        seekBar.setMaxValue(memberType.age_max.toFloat())
        tvStartValue.text = memberType.age_min.toString()
        tvEndValue.text = memberType.age_max.toString()

        dialog.show()
    }

    fun showAgeDialog(
        ctx: Context,
        ageMin: Int,
        ageMax: Int,
        onPositiveClick: (min: Int, max: Int) -> Unit
    ) {
        val dialog = Dialog(ctx)
        dialog.setContentView(R.layout.dialog_age_filter)
        val seekBar = dialog.findViewById<CrystalRangeSeekbar>(R.id.seekbar_age)
        val btnOk = dialog.findViewById<Button>(R.id.btn_ok)
        val btnCancel = dialog.findViewById<Button>(R.id.btn_cancel)
        val tvStartValue = dialog.findViewById<TextView>(R.id.tv_start_value)
        val tvEndValue = dialog.findViewById<TextView>(R.id.tv_end_value)

        seekBar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            tvStartValue.text = minValue.toString()
            tvEndValue.text = maxValue.toString()
        }
        btnCancel.setOnClickListener { dialog.dismiss() }
        btnOk.setOnClickListener {
            onPositiveClick(seekBar.selectedMinValue.toInt(), seekBar.selectedMaxValue.toInt())
            dialog.dismiss()
        }

        seekBar.setMinValue(ageMin.toFloat())
        seekBar.setMaxValue(ageMax.toFloat())
        tvStartValue.text = ageMin.toString()
        tvEndValue.text = ageMax.toString()

        dialog.show()
    }

    fun showCountriesDialog(
        ctx: Context,
        sexList: ArrayList<CountryModel>,
        onPositiveClick: (country: CountryModel) -> Unit
    ) {
        val items: Array<String> = sexList.map { it.name }.toTypedArray()

        sexList.forEachIndexed { index, sex ->
            items[index] = sex.name
        }

        val builder = AlertDialog.Builder(ctx)
        builder.setSingleChoiceItems(items, 0, null)
            .setPositiveButton(android.R.string.ok) { dialog, id ->
                val selectedPosition = (dialog as AlertDialog).listView.checkedItemPosition
                onPositiveClick(sexList[selectedPosition])
                dialog.dismiss()
            }.setNegativeButton(android.R.string.cancel) { dialog, id ->
                dialog.dismiss()
            }.show()
    }

    fun showCitiesDialog(
        ctx: Context,
        list: ArrayList<City>,
        onPositiveClick: (city: City) -> Unit
    ) {
        val items: Array<String> = list.map { it.name }.toTypedArray()

        list.forEachIndexed { index, city ->
            items[index] = city.name
        }

        val builder = AlertDialog.Builder(ctx)
        builder.setSingleChoiceItems(items, 0, null)
            .setPositiveButton(android.R.string.ok) { dialog, id ->
                val selectedPosition = (dialog as AlertDialog).listView.checkedItemPosition
                onPositiveClick(list[selectedPosition])
                dialog.dismiss()
            }.setNegativeButton(android.R.string.cancel) { dialog, id ->
                dialog.dismiss()
            }.show()
    }

    @SuppressLint("SetTextI18n")
    fun showDeleteDialog(title: String, fragment: BaseFragment, onDeleteClick: () -> Unit) {
        val dialogBuilder = AlertDialog.Builder(fragment.context)
        val dialogView = fragment.layoutInflater.inflate(R.layout.dialog_delete_event, null)

        dialogBuilder.setView(dialogView)

        val dialog = dialogBuilder.create()

        dialogView.findViewById<CardView>(R.id.deleteBtn).setOnClickListener {
            onDeleteClick()
            dialog.hide()
        }
        dialogView.findViewById<CardView>(R.id.cancelBtn).setOnClickListener {
            dialog.hide()
        }
        dialogView.findViewById<TextView>(R.id.tv_title).text =
            fragment.context?.getString(R.string.delete_event_dialog_title) + title

        dialog.show()
    }

}