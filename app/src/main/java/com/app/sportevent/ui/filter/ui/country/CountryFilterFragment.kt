package com.app.sportevent.ui.filter.ui.country

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.sportevent.R
import com.app.sportevent.data.poko.CountryModel
import com.app.sportevent.data.poko.SportKindModel
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.data.util.AppTextWatcher
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.filter.FilterModel
import kotlinx.android.synthetic.main.fragment_filter_base.*
import org.koin.android.ext.android.inject

class CountryFilterFragment : BaseFragment() {

    private val model: FilterModel by inject()
    private val adapter = CountryListAdapter()
    private var countryList: ArrayList<CountryModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_filter_base, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
        countryList = if (model.countryList.isEmpty())
            model.filterData.country
        else model.countryList

        adapter.setItems(countryList)
    }

    private fun setupUi() {
        iv_back.setOnClickListener {  AppFragmentManager.closeFragment(activity!!) }
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        tv_toolbar_title.text = "Страна"

        rv_sport_list.layoutManager = LinearLayoutManager(activity!!)
        rv_sport_list.adapter = adapter

        btn_reset_filter.setOnClickListener {
            countryList.forEach { it.isSelected = false }
            model.countryList = countryList
            adapter.setItems(countryList)
            model.filter {  }
        }

        et_search.addTextChangedListener(object : AppTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null) adapter.filter.filter(s)
            }
        })

        adapter.onItemCheck = { kind, isChecked ->
            countryList.forEach { if (it.id == kind.id) it.isSelected = isChecked }
            model.countryList = countryList
            model.getCityListInCounty()
            model.filter{}
        }
    }
}