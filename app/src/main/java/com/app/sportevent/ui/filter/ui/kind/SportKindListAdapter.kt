package com.app.sportevent.ui.filter.ui.kind

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.app.sportevent.R
import com.app.sportevent.data.BASE_URL
import com.app.sportevent.data.ENDPOINT_ICON
import com.app.sportevent.data.ENDPOINT_IMAGES
import com.app.sportevent.data.poko.SportKindModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_sport_filter.view.*
import timber.log.Timber
import java.lang.Exception


class SportKindListAdapter : RecyclerView.Adapter<Holder>(), Filterable {

    private var items: ArrayList<SportKindModel> = ArrayList()
    var onItemCheck: (sportKind: SportKindModel, isChecked: Boolean) -> Unit = { _, _ -> }
    var onNothingFound: () -> Unit = {}
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        Holder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_sport_filter, parent, false)
        )

    override fun getItemCount() = items.size

    fun setItems(items: ArrayList<SportKindModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item = items[position]

        holder.itemView.apply {
            tv_sport_title.text = item.name

            val url = BASE_URL + ENDPOINT_ICON + item.icon
            Picasso
                .get()
                .load(url)
                .into(ic, object : Callback{
                    override fun onError(e: Exception?) {
                        e?.printStackTrace()
                    }

                    override fun onSuccess() {
                        Timber.i("")
                    }
                })

            selector.isChecked = item.isSelected
            this.setOnClickListener {
                selector.isChecked = !selector.isChecked
            }
            selector.setOnCheckedChangeListener { btn, isChecked ->
                onItemCheck(item, isChecked)
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            private var filterResultList: ArrayList<SportKindModel> = ArrayList()
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val results = FilterResults()
                if (constraint!!.isNotEmpty()) {
                    items.forEach {
                        if (it.name.toLowerCase().contains(constraint.toString()))
                            filterResultList.add(it)
                    }
                    results.values = filterResultList
                    results.count = filterResultList.size
                }
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results?.count == 0) this@SportKindListAdapter.setItems(items)
                else this@SportKindListAdapter.setItems(filterResultList)
                notifyDataSetChanged()
            }
        }
    }
}

class Holder(view: View) : RecyclerView.ViewHolder(view)