package com.app.sportevent.ui.auth.base

import com.app.sportevent.data.Api
import com.app.sportevent.data.NO_CONNECTION_TEXT
import com.app.sportevent.data.SERVER_RESPONSE_OK
import com.app.sportevent.data.poko.CityResponse
import com.app.sportevent.data.poko.request.FbAuthRequest
import com.app.sportevent.data.poko.request.FirebaseAuthRequest
import com.app.sportevent.data.poko.request.LocationRequest
import com.app.sportevent.data.poko.request.VkAuthRequest
import com.app.sportevent.data.util.FirebaseProvider
import com.app.sportevent.data.util.PrefManager
import com.facebook.login.LoginResult
import com.vk.api.sdk.auth.VKAccessToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.ConnectException

open class AuthModel(private val prefManager: PrefManager) {

    open val api = Api.get()
    open var onError: (message: String?) -> Unit = {}

    open fun registerWithFirebase(
        token: String,
        countryId: Int,
        onRegister: (isRegistered: Boolean) -> Unit
    ) {
        FirebaseProvider.provideFireBaseToken { firebaseToken ->
            GlobalScope.launch {
                val request = FirebaseAuthRequest(
                    firebase_user_id = token,
                    device_token = firebaseToken,
                    country_id = countryId,
                    device = 0,
                    lang = "RU"
                )
                val fieldMap: HashMap<String, Any?> = HashMap()
                fieldMap["firebase_user_id"] = request.firebase_user_id
                fieldMap["device_token"] = request.device_token
                fieldMap["country_id"] = request.country_id
                fieldMap["device"] = request.device
                fieldMap["lang"] = request.lang
                try {
                    val response = api.firebaseSignIn(fieldMap).await()
                    if (response.isSuccessful) {
                        if (response.body()!!.status == SERVER_RESPONSE_OK) {
                            prefManager.saveToken(response.body()!!.token)
                            onRegister(true)
                        } else onError(response.body()!!.comment + "")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } catch (e: ConnectException) {
                    onError(NO_CONNECTION_TEXT)
                }
            }
        }
    }

    open fun registerWithVk(
        token: VKAccessToken,
        countryId: Int,
        onRegister: (isRegistered: Boolean) -> Unit
    ) {
        FirebaseProvider.provideFireBaseToken { firebaseToken ->
            GlobalScope.launch {
                val request = VkAuthRequest(
                    vk_user_id = token.userId!!,
                    device_token = firebaseToken,
                    country_id = countryId,
                    device = 0,
                    email = token.email!!,
                    lang = "RU"
                )
                val fieldMap: HashMap<String, Any?> = HashMap()
                fieldMap["vk_user_id"] = request.vk_user_id
                fieldMap["device_token"] = request.device_token
                fieldMap["country_id"] = request.country_id
                fieldMap["device"] = request.device
                fieldMap["lang"] = request.lang
                try {
                    val response = api.vkSignIn(fieldMap).await()
                    if (response.isSuccessful) {
                        if (response.body()!!.status == SERVER_RESPONSE_OK) {
                            prefManager.saveToken(response.body()!!.token)
                            onRegister(true)
                        } else onError(response.body()!!.comment + "")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } catch (e: ConnectException) {
                    onError(NO_CONNECTION_TEXT)
                }
            }
        }
    }

    open fun registerWithFb(
        loginResult: LoginResult,
        countryId: Int,
        onRegister: (isRegistered: Boolean) -> Unit
    ) {
        FirebaseProvider.provideFireBaseToken { firebaseToken ->
            GlobalScope.launch {
                val request = FbAuthRequest(
                    fb_token = loginResult.accessToken.token,
                    device_token = firebaseToken,
                    country_id = countryId,
                    device = 0,
                    lang = "RU"
                )
                val fieldMap: HashMap<String, Any?> = HashMap()
                fieldMap["fb_token"] = request.fb_token
                fieldMap["device_token"] = request.device_token
                fieldMap["country_id"] = request.country_id
                fieldMap["device"] = request.device
                fieldMap["lang"] = request.lang
                try {
                    val response = api.fbSignIn(fieldMap).await()
                    if (response.isSuccessful) {
                        if (response.body()!!.status == SERVER_RESPONSE_OK) {
                            prefManager.saveToken(response.body()!!.token)
                            withContext(Dispatchers.Main) { onRegister(true) }
                        } else withContext(Dispatchers.Main) { onError(response.body()!!.comment + "") }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } catch (e: ConnectException) {
                    onError(NO_CONNECTION_TEXT)
                }
            }
        }
    }

    fun getCountryByLocation(
        latitude: Double,
        longitude: Double,
        onFetched: (city: CityResponse) -> Unit
    ) {
        GlobalScope.launch {
            val request = LocationRequest(latitude, longitude, "RU")
            val fieldMap: HashMap<String, Any?> = HashMap()
            fieldMap["lat"] = request.lat
            fieldMap["lng"] = request.lng
            fieldMap["lang"] = request.lang
            try {
                val response = api.getCityByGeo(fieldMap).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) { onFetched(response.body()!!) }
                    else onError(response.body()!!.comment)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }
}