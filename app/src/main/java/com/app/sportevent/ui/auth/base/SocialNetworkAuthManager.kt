package com.app.sportevent.ui.auth.base

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.app.sportevent.R
import com.app.sportevent.data.RC_SIGN_IN
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import com.vk.api.sdk.auth.VKScope
import timber.log.Timber

object SocialNetworkAuthManager {
    private lateinit var googleSignInClient: GoogleSignInClient
    private val fbCallbackManager = CallbackManager.Factory.create()
    private lateinit var activity: AppCompatActivity

    fun init(activity: AppCompatActivity) {
        SocialNetworkAuthManager.activity = activity
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(activity.getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(activity, gso)
    }

    fun requestAuthWithGoogle() {
        val signInIntent = googleSignInClient.signInIntent
        activity.startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    fun requestAuthWithVk() {
        VK.login(activity, arrayListOf(VKScope.WALL, VKScope.PHOTOS))
    }

    fun requestAuthWithFb(onSuccess: (loginResult: LoginResult) -> Unit) {
        LoginManager.getInstance().registerCallback(
            fbCallbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    onSuccess(loginResult)
                }

                override fun onCancel() {
                    Timber.e("Vk auth error")
                }

                override fun onError(error: FacebookException?) {
                    Timber.e("Vk auth error :$error")
                }
            })
        LoginManager.getInstance().logInWithReadPermissions(activity, listOf("email"))
    }

    fun handleGoogleAuthResponse(data: Intent?, onSuccess: (uid: String) -> Unit) {
        val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
        val acct = result?.signInAccount
        if (acct != null) firebaseAuthWithGoogle(acct) {
            onSuccess(it)
        }
    }

    fun handleVkAuthResponse(
        requestCode: Int,
        resultCode: Int,
        data: Intent?,
        onLogin: (token: VKAccessToken) -> Unit
    ): Boolean {
        val callback = object : VKAuthCallback {
            override fun onLogin(token: VKAccessToken) {
                onLogin(token)
            }

            override fun onLoginFailed(errorCode: Int) {
                Timber.e("Vk auth error :$errorCode")
            }
        }
        return !(data == null || !VK.onActivityResult(requestCode, resultCode, data, callback))
    }

    private fun firebaseAuthWithGoogle(
        acct: GoogleSignInAccount,
        onSuccess: (uid: String) -> Unit
    ) {
        val fbAuth = FirebaseAuth.getInstance()
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        fbAuth.signInWithCredential(credential).addOnCompleteListener(activity) { task ->
            if (task.isSuccessful) onSuccess(fbAuth.currentUser!!.uid)
        }
    }

    fun notifyActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        fbCallbackManager.onActivityResult(requestCode, resultCode, data)

    }
}