package com.app.sportevent.ui.event.create.ui.preview.page

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.sportevent.R
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.data.util.DateUtil
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.create.CreateEventModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_create_event_first.*
import kotlinx.android.synthetic.main.fragment_event_preview_general.*
import kotlinx.android.synthetic.main.fragment_event_preview_general.tv_event_type
import kotlinx.android.synthetic.main.fragment_event_preview_general.tv_sex
import org.jetbrains.anko.textColor
import org.koin.android.ext.android.inject
import timber.log.Timber
import java.io.File

class CreateEventPreviewGeneralFragment : BaseFragment() {

    private val model: CreateEventModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_event_preview_general, container, false)!!

    override fun onResume() {
        super.onResume()
        setupUi()
    }

    private fun setupUi() {
        setOrganizerToView()
        setSexToView()
        setParticipantsToView()
        setDateToView()
        setEventTypeToView()
        setImageToView()
        setDescriptionToView()
        tv_memver_count.text = "${model.memberCount} мест"
        tv_city.text = model.selectedCity.name
        tv_country.text = model.selectedCountry.name
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
    }

    private fun setDescriptionToView() {
        tv_description.text = model.description
    }

    private fun setOrganizerToView() {
        tv_organizer.text = model.currentUser.name
    }

    private fun setImageToView() {
        if (model.imagePath != null) {
            Picasso
                .get()
                .load(File(model.imagePath!!))
                .resize(0,400)
                .into(iv_logo, object : Callback {
                    override fun onError(e: Exception?) {
                        Timber.e(e, "error")
                    }

                    override fun onSuccess() {
                        Timber.i("success")
                    }
                })
        }
    }

    private fun setEventTypeToView() {
        if (model.selectedEventType.id != 0) {
            tv_event_type.textColor = Color.BLACK
            tv_event_type.text = model.selectedEventType.name
        }
    }

    private fun setDateToView() {
        val startM = model.startDate
        val endM = model.endDate
        if (startM != 0L && endM != 0L) DateUtil.apply {
            val startDate = "${millisToFormattedDay(startM)}.${millisToFormattedMonth(startM)}.${
                millisToYear(startM)
            }"
            val endDate =
                " ${millisToFormattedDay(endM)}.${millisToFormattedMonth(endM)}.${millisToYear(endM)}"
            tv_date.text = "$startDate - $endDate"
        }
    }

    private fun setParticipantsToView() {
        val memberTypeText = model.selectedMemberType.name
        val memberAgeText = "${model.ageMin}-${model.ageMax}"
        tv_participants.text = "$memberTypeText $memberAgeText"
    }

    private fun setSexToView() {
        val list = model.sexList
        var text = ""
        val selectedSexList = list.filter { it.isChecked }
        if (selectedSexList.size == 1) text = selectedSexList[0].title
        else selectedSexList.forEachIndexed { index, sex ->
            val title = if (index != list.size - 1) "${sex.title}," else sex.title
            if (sex.isChecked) text = "$text $title"
        }
        if (text.isNotEmpty()) {
            tv_sex.textColor = Color.BLACK
            tv_sex.text = text
        }
    }
}