package com.app.sportevent.ui.event.create

import android.content.Context
import com.app.sportevent.data.Api
import com.app.sportevent.data.LANG_RU
import com.app.sportevent.data.NO_CONNECTION_TEXT
import com.app.sportevent.data.SERVER_RESPONSE_OK
import com.app.sportevent.data.builder.DictionariesBuilder
import com.app.sportevent.data.poko.*
import com.app.sportevent.data.util.FileUtil
import com.app.sportevent.data.util.PrefManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.ConnectException

class CreateEventModel(context: Context) {

    private val prefManager = PrefManager(context)
    private val api = Api.get()
    var onError: (message: String?) -> Unit = {}

    private var countryList: ArrayList<CountryModel> = ArrayList()
    private var cityList: ArrayList<City> = ArrayList()
    private var filterData: DictionariesResponse = DictionariesBuilder.empty()

    var title = ""
    var selectedSportKind: SportKindModel = SportKindModel()
    var selectedEventType = TypeEventModel(0, "", false)
    var selectedMemberType = TypeParticipantModel(0, 0, 0, "", false)
    var ageMax = 0
    var ageMin = 0

    var selectedCountry = CountryModel(-1, "", "")
    var selectedCity = City(-1, "", 0.0, 0.0, "")
    var startDate = 0L
    var endDate = 0L
    var description = ""

    var memberCount = 0
    var memberPayment = ""
    var includedToPayment = ""
    var prizePool = ""

    var imagePath: String? = null
    var currentUser = User()

    var sexList = arrayListOf(
        Sex(1, "Мужской", false),
        Sex(2, "Женский", false)
    )

    private fun getSelectedSexId(): Int {
        return if (sexList[0].isChecked && sexList[1].isChecked) 0
        else if (sexList[0].isChecked) sexList[0].sex
        else if (sexList[1].isChecked) sexList[1].sex
        else -1
    }

    fun getFilterData() = filterData
    fun getCountryList() = countryList
    fun getCityList() = cityList

    fun initFilterData(onFetched: (dictionaries: DictionariesResponse) -> Unit) {
        GlobalScope.launch {
            try {
                val response = api.getCommonData(prefManager.getToken(), LANG_RU).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            filterData = response.body()!!
                            onFetched(response.body()!!)
                        }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    fun initCountryList() {
        GlobalScope.launch {
            try {
                val response = api.getCountriesList(LANG_RU).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            countryList = response.body()!!.countries
                        }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    fun getCityListInCounty() {
        GlobalScope.launch {
            try {
                val response = api.getCityListByCountry(
                    lang = LANG_RU,
                    word = "",
                    isoList = arrayListOf(selectedCountry.iso)
                ).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            cityList = response.body()!!.cities
                        }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    fun createEvent(onSuccess: (isCreated: Boolean) -> Unit) {
        GlobalScope.launch {
            try {
                val response = Api.get().createEvent(
                    token = prefManager.getToken(),
                    name = title,
                    sportKindId = selectedSportKind.id,
                    typeEventId = selectedEventType.id,
                    sex = getSelectedSexId(),
                    memberType = selectedMemberType.id,
                    countyId = selectedCountry.id,
                    cityId = selectedCity.id,
                    startDate = startDate / 1000,
                    endDate = endDate / 1000,
                    description = description,
                    memberCount = memberCount,
                    memberPayment = memberPayment,
                    includedToPayment = includedToPayment,
                    prizePool = prizePool,
                    ageMax = ageMax,
                    ageMin = ageMin,
                    image = FileUtil.convertImageToBase64(imagePath)
                ).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            resetGlobalVar()
                            onSuccess(true)
                        }
                    else withContext(Dispatchers.Main) {
                        onSuccess(false)
                    }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    fun getCurrentUser(onFetched: (user: UserResponse) -> Unit) {
        GlobalScope.launch {
            try {
                val response = Api.get().getUserInfo(prefManager.getToken()).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            val user = response.body()!!
                            currentUser = user.user
                            onFetched(user)
                        }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    private fun resetGlobalVar() {
        title = ""
        selectedSportKind = SportKindModel(0, "", "")
        selectedEventType = TypeEventModel(0, "", false)
        selectedMemberType = TypeParticipantModel(0, 0, 0, "", false)
        ageMax = 0
        ageMin = 0
        selectedCountry = CountryModel(-1, "", "")
        selectedCity = City(-1, "", 0.0, 0.0, "")
        startDate = 0L
        endDate = 0L
        description = ""
        memberCount = 0
        memberPayment = ""
        includedToPayment = ""
        prizePool = ""
        currentUser = User()
        sexList.forEach { it.isChecked = false }
        imagePath = null
    }
}