package com.app.sportevent.ui.list

import com.app.sportevent.data.Api
import com.app.sportevent.data.NO_CONNECTION_TEXT
import com.app.sportevent.data.SERVER_RESPONSE_OK
import com.app.sportevent.data.poko.Event
import com.app.sportevent.data.util.PrefManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.ConnectException

class EventListModel(private val prefManager: PrefManager) {

    var onError: (message: String?) -> Unit = {}

    fun getEventList(onFetched: (list: ArrayList<Event>) -> Unit) {
        GlobalScope.launch {
            try {
                val response = Api.get().getEventList(prefManager.getToken()).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            onFetched(response.body()!!.events)
                        }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }
}