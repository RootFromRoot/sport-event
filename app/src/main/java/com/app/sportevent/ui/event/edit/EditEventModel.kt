package com.app.sportevent.ui.event.edit

import com.app.sportevent.data.Api
import com.app.sportevent.data.LANG_RU
import com.app.sportevent.data.NO_CONNECTION_TEXT
import com.app.sportevent.data.SERVER_RESPONSE_OK
import com.app.sportevent.data.builder.DictionariesBuilder
import com.app.sportevent.data.poko.*
import com.app.sportevent.data.util.FileUtil
import com.app.sportevent.data.util.PrefManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.ConnectException

class EditEventModel(private val prefManager: PrefManager) {

    var filterData: DictionariesResponse = DictionariesBuilder.empty()
        private set
    var countryList: ArrayList<CountryModel> = ArrayList()
    var cityList: ArrayList<City> = ArrayList()

    var event = Event()
    var onError: (message: String?) -> Unit = {}

    var sexList = arrayListOf(
        Sex(1, "Мужской", false),
        Sex(2, "Женский", false)
    )

    var startTimeInMilliseconds: Long? = 0L
    var endTimeInMilliseconds: Long? = 0L

    var imagePath: String? = null

    fun updateEvent(onSuccess: (isCreated: Boolean) -> Unit) {
        GlobalScope.launch {
            try {
                val response = Api.get().updateEvent(
                    id = event.id,
                    token = prefManager.getToken(),
                    name = event.name,
                    sportKindId = event.sportKind?.id,
                    typeEventId = event.eventType?.id,
                    sex = getSelectedSexId(),
                    memberType = event.memberType?.id,
                    countyId = event.country?.id,
                    cityId = event.city?.id,
                    startDate = startTimeInMilliseconds?.div(1000),
                    endDate = endTimeInMilliseconds?.div(1000),
                    description = event.description,
                    memberCount = event.participant_count,
                    memberPayment = event.participant_sum,
                    includedToPayment = event.description_ante,
                    prizePool = event.prize_pool,
                    ageMax = event.age_max,
                    ageMin = event.age_min,
                    image = event.icon
                ).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            resetGlobalVar()
                            onSuccess(true)
                        }
                    else withContext(Dispatchers.Main) {
                        onSuccess(false)
                    }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    private fun resetGlobalVar() {
        event = Event()
        imagePath = null
        startTimeInMilliseconds = 0L
        endTimeInMilliseconds = 0L
        sexList.forEach { it.isChecked = false }
    }

    private fun getSelectedSexId(): Int {
        return if (sexList[0].isChecked && sexList[1].isChecked) 0
        else if (sexList[0].isChecked) sexList[0].sex
        else if (sexList[1].isChecked) sexList[1].sex
        else -1
    }

    fun getCurrentEvent(eventId: Int, onFetched: (event: Event) -> Unit) {
        GlobalScope.launch {
            try {
                val response = Api.get().getEventList(prefManager.getToken()).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            val events = response.body()!!.events
                            val event = events.find { it.id == eventId }!!
                            this@EditEventModel.event = event
                            initVariables(event)
                            onFetched(event)
                        }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    private fun initVariables(event: Event) {
        initSexList(event.gender)
        initTimeVars(event)
    }

    private fun initTimeVars(event: Event) {
        startTimeInMilliseconds = event.date_start?.times(1000)
        endTimeInMilliseconds = event.date_stop?.times(1000)
    }

    private fun initSexList(gender: Int?) {
        when (gender) {
            0 -> {
                sexList[0].isChecked = true
                sexList[1].isChecked = true
            }
            1 -> sexList[0].isChecked = true
            2 -> sexList[1].isChecked = true
        }
    }

    fun initFilterData(onFetched: (dictionaries: DictionariesResponse) -> Unit) {
        GlobalScope.launch {
            try {
                val response = Api.get().getCommonData(prefManager.getToken(), LANG_RU).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            filterData = response.body()!!
                            onFetched(response.body()!!)
                        }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    fun initCountryList() {
        GlobalScope.launch {
            try {
                val response = Api.get().getCountriesList(LANG_RU).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            countryList = response.body()!!.countries
                        }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    fun getCityListInCounty() {
        GlobalScope.launch {
            try {
                val response = Api.get().getCityListByCountry(
                    lang = LANG_RU,
                    word = "",
                    isoList = arrayListOf(event.country!!.iso)
                ).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            cityList = response.body()!!.cities
                        }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    fun setImageToEvent(selectedImage: GalleryImage) {
        event.icon = FileUtil.convertImageToBase64(selectedImage.path)
    }

}
