package com.app.sportevent.ui.auth.base

import com.facebook.login.LoginResult
import com.vk.api.sdk.auth.VKAccessToken

open class AuthPresenter(private val model: AuthModel) {

    lateinit var authView: AuthView

    open fun bindAuthView(authView: AuthView) {
        this.authView = authView
        model.onError = { authView.showErrorToast("Error $it") }
    }

    open fun userWantAuthWithVk(token: VKAccessToken, countryId: Int) {
        if (token.email != null)
            model.registerWithVk(token, countryId) {
                if (it) authView.showPostListScreen()
                else authView.showAuthErrorMsg()
                authView.closeProgressDialog()
            }
        else authView.showVkNoEmailToast()
    }

    open fun userWantAuthWithFb(loginResult: LoginResult, selectedCountryId: Int) {
        model.registerWithFb(loginResult, selectedCountryId) {
            if (it) authView.showPostListScreen()
            else authView.showAuthErrorMsg()
            authView.closeProgressDialog()
        }
    }

    open fun userWantAuthWithGoogle(uid: String, selectedCountryId: Int) {
        model.registerWithFirebase(uid, selectedCountryId) {
            if (it) authView.showPostListScreen()
            else authView.showAuthErrorMsg()
            authView.closeProgressDialog()
        }
    }

    fun getCountryByLocation(latitude: Double, longitude: Double) {
        model.getCountryByLocation(latitude,longitude){
            authView.setCityToView(it)
        }
    }
}