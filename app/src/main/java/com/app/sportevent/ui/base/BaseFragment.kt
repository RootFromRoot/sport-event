package com.app.sportevent.ui.base

import androidx.fragment.app.Fragment
import com.app.sportevent.ui.MainActivity

open class BaseFragment : Fragment() {
    fun activity() = (activity as MainActivity)
}