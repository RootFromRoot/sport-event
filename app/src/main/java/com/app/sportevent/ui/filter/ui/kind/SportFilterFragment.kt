package com.app.sportevent.ui.filter.ui.kind

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.sportevent.R
import com.app.sportevent.data.poko.SportKindModel
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.data.util.AppTextWatcher
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.filter.FilterModel
import kotlinx.android.synthetic.main.fragment_filter_base.*
import org.koin.android.ext.android.inject

class SportFilterFragment : BaseFragment() {

    private val model: FilterModel by inject()
    private val adapter = SportKindListAdapter()
    private var sportKindList: ArrayList<SportKindModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_filter_base, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
        sportKindList = if (model.sportKindList.isEmpty())
            model.filterData.sportKind
        else model.sportKindList

        adapter.setItems(sportKindList)
    }

    private fun setupUi() {
        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        tv_toolbar_title.text = "Виды спорта"

        rv_sport_list.layoutManager = LinearLayoutManager(activity!!)
        rv_sport_list.adapter = adapter

        btn_reset_filter.setOnClickListener {
            sportKindList.forEach { it.isSelected = false }
            model.sportKindList = sportKindList
            adapter.setItems(sportKindList)
            model.filter { }
        }

        et_search.addTextChangedListener(object : AppTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null) adapter.filter.filter(s)
            }
        })

        adapter.onItemCheck = { kind, isChecked ->
            sportKindList.forEach { if (it.id == kind.id) it.isSelected = isChecked }
            model.sportKindList = sportKindList
            model.filter {}
        }
    }
}