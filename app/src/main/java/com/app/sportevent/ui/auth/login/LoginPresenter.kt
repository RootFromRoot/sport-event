package com.app.sportevent.ui.auth.login

import com.app.sportevent.ui.auth.base.AuthPresenter
import com.app.sportevent.ui.auth.base.AuthView
import com.app.sportevent.ui.auth.registration.RegistrationView

class LoginPresenter(private val model: LoginModel) : AuthPresenter(model){

    private lateinit var view: LoginView

     fun bind(view: LoginView) {
        this.view = view
         model.onError = { view.showErrorToast("Error $it") }
    }

    fun userWantLogin(email: String, pass: String){
        model.login(email, pass){
            view.closeProgressDialog()
            view.showPostListScreen()
        }
    }

}