package com.app.sportevent.ui.auth.base

import com.app.sportevent.data.poko.CityResponse

interface AuthView {
    fun showAuthErrorMsg()
    fun closeProgressDialog()
    fun showPostListScreen()
    fun showErrorToast(text: String)
    fun showVkNoEmailToast()
    fun setCityToView(city: CityResponse)
}