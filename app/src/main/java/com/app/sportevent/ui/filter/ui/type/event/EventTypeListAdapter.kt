package com.app.sportevent.ui.filter.ui.type.event

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.app.sportevent.R
import com.app.sportevent.data.poko.TypeEventModel
import kotlinx.android.synthetic.main.item_sport_filter.view.*

class EventTypeListAdapter : RecyclerView.Adapter<Holder>() {

    private var items: ArrayList<TypeEventModel> = ArrayList()
    var onItemCheck: (eventType: TypeEventModel, isChecked: Boolean) -> Unit = { _, _ -> }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        Holder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.list_item_base_string, parent, false)
        )

    override fun getItemCount() = items.size

    fun setItems(items: ArrayList<TypeEventModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item = items[position]

        holder.itemView.apply {
            tv_sport_title.text = item.name
            selector.isChecked = item.isSelected
            this.setOnClickListener {
                selector.isChecked = !selector.isChecked
            }
            selector.setOnCheckedChangeListener { btn, isChecked ->
                onItemCheck(item, isChecked)
            }
        }
    }
}

class Holder(view: View) : RecyclerView.ViewHolder(view)