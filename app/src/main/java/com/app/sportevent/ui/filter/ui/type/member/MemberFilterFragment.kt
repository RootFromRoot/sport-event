package com.app.sportevent.ui.filter.ui.type.member

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.sportevent.R
import com.app.sportevent.data.poko.TypeParticipantModel
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.filter.FilterModel
import kotlinx.android.synthetic.main.fragment_filter_base.*
import org.koin.android.ext.android.inject

class MemberFilterFragment : BaseFragment() {

    private val model: FilterModel by inject()
    private val adapter = MemberTypeListAdapter()
    private var memberTypeList: ArrayList<TypeParticipantModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_filter_base, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
        initViews()
    }

    private fun initViews() {
        memberTypeList = if (model.memberTypeList.isEmpty())
            model.filterData.typeParticipant
        else model.memberTypeList

        adapter.setItems(memberTypeList)
    }

    private fun setupUi() {
        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        tv_toolbar_title.text = "Тип участников"

        btn_reset_filter.setOnClickListener {
            memberTypeList.forEach { it.isSelected = false }
            model.memberTypeList = memberTypeList
            adapter.setItems(memberTypeList)
            model.filter { }
        }

        rv_sport_list.layoutManager = LinearLayoutManager(activity!!)
        rv_sport_list.adapter = adapter

        adapter.onItemCheck = { type, isChecked ->
            memberTypeList.forEach { if (it.id == type.id) it.isSelected = isChecked }
            model.memberTypeList = memberTypeList
            model.filter {}
        }
    }

}
