package com.app.sportevent.ui.auth.restore

import android.app.ProgressDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.sportevent.R
import kotlinx.android.synthetic.main.activity_reset_password.*
import org.jetbrains.anko.toast

class ResetPasswordActivity : AppCompatActivity() {

    private val model = RestorePasswordModel()
    private lateinit var progressDialog: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        progressDialog = ProgressDialog(this)
        setupUi()
    }

    private fun setupUi() {
        iv_back.setOnClickListener { onBackPressed() }
        restoreBtn.setOnClickListener { if (isEtValid()) restorePassword() }
    }

    private fun restorePassword() {

        progressDialog.setMessage("Сброс пароля...")
        progressDialog.show()
        model.onError = {
            progressDialog.dismiss()
            toast("Введен некорректный адрес email")
        }
        model.restore(emailField.text.toString().trim()) {
            progressDialog.dismiss()
            toast("На ваш email отправлено письмо для восстановления пароля")
            onBackPressed()
        }
    }

    private fun isEtValid(): Boolean {
        return if (emailField.text.isNullOrEmpty()) {
            toast("Введите e-mail")
            false
        } else if (!emailField.text.toString().contains("@")) {
            toast("Не валидный e-mail")
            false
        } else true
    }
}
