package com.app.sportevent.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.app.sportevent.R
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.ui.list.ui.EventListFragment
import com.app.sportevent.ui.message.MessagesFragment
import com.app.sportevent.ui.profile.UserProfileFragment
import com.app.sportevent.ui.request.RequestsFragment
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber


class MainActivity : AppCompatActivity() {

    var onBackClick: () -> Unit = {}
    var openedFrom = ""
    private val eventListFragment = EventListFragment()
    private val messagesFragment = MessagesFragment()
    private val userProfileFragment = UserProfileFragment()
    private val requestsFragment = RequestsFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupUi()
    }

    private fun setupUi() {
        nav_view.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_events -> reshowFragment(eventListFragment)
                R.id.menu_messages -> reshowFragment(messagesFragment)
                R.id.menu_profile -> reshowFragment(userProfileFragment)
                R.id.menu_requests -> reshowFragment(requestsFragment)
            }
            return@setOnNavigationItemSelectedListener true
        }
        nav_view.setOnNavigationItemReselectedListener {
            when (it.itemId) {
                R.id.menu_events -> reshowFragment(eventListFragment)
                R.id.menu_messages -> reshowFragment(messagesFragment)
                R.id.menu_profile -> reshowFragment(userProfileFragment)
                R.id.menu_requests -> reshowFragment(requestsFragment)
            }
        }
        setSelectedListFragment()
    }

    fun setSelectedListFragment() {
        nav_view.selectedItemId = R.id.menu_events
    }

    fun setSelectedMyProfileFragment() {
        nav_view.selectedItemId = R.id.menu_profile
    }

    private fun reshowFragment(fragment: Fragment) {
        AppFragmentManager.openFragmentWithoutBackStark(this, fragment)
    }

    private fun showFragment(fragment: Fragment) {
        AppFragmentManager.openFragment(this, fragment)
//        AppFragmentManager.openFragmentWithoutBackStark(this, fragment)
        /* val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
         val currentFragment = supportFragmentManager.primaryNavigationFragment
         if (currentFragment != null) fragmentTransaction.hide(currentFragment)

         var fragmentTemp = supportFragmentManager.findFragmentByTag(fragment.javaClass.name)
         when (fragmentTemp) {
             null -> {
                 fragmentTemp = fragment
                 fragmentTransaction.add(R.id.container, fragmentTemp, fragment.javaClass.name)
             }
             else -> fragmentTransaction.show(fragmentTemp)
         }

         fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp)
         fragmentTransaction.setReorderingAllowed(true)
         fragmentTransaction.commitNowAllowingStateLoss()*/
    }

    override fun onBackPressed() {
        onBackClick()
        val fr1 = supportFragmentManager.findFragmentByTag(eventListFragment.javaClass.name)
        val fr2 = supportFragmentManager.findFragmentByTag(messagesFragment.javaClass.name)
        val fr3 = supportFragmentManager.findFragmentByTag(userProfileFragment.javaClass.name)
        val fr4 = supportFragmentManager.findFragmentByTag(requestsFragment.javaClass.name)


        /*if (supportFragmentManager.backStackEntryCount == 1 && fr1 != null)
            finish()
        else if (supportFragmentManager.backStackEntryCount == 1) {
            if (fr1 != null || fr2 != null || fr3 != null || fr4 != null) {
                Timber.i("")
            } else
                super.onBackPressed()

        } else
            supportFragmentManager.popBackStack()*/

     /*   if (supportFragmentManager.backStackEntryCount > 1) supportFragmentManager.popBackStack()
        else {
        }*/
/*
          if (fr1 != null || fr2 != null || fr3 != null || fr4 != null) {
              supportFragmentManager.popBackStack()
          } else if (getVisibleFragment() != null) {
              AppFragmentManager.closeFragment(this)
          } else super.onBackPressed()*/
    }

    private fun getVisibleFragment(): Fragment? {
        supportFragmentManager.fragments.forEach { fragment ->
            if (fragment != null && fragment.isVisible) return fragment
        }
        return null
    }
}
