package com.app.sportevent.ui.event.create.ui

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.sportevent.R
import com.app.sportevent.data.poko.City
import com.app.sportevent.data.poko.CountryModel
import com.app.sportevent.data.toast
import com.app.sportevent.data.util.*
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.create.CreateEventModel
import com.app.sportevent.ui.filter.ui.date.EndDateFragment
import com.app.sportevent.ui.filter.ui.date.StartDateFragment
import kotlinx.android.synthetic.main.fragment_create_event_second.*
import org.jetbrains.anko.textColor
import org.koin.android.ext.android.inject

class CreateEventSecondFragment : BaseFragment() {

    private val model: CreateEventModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_create_event_second, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
    }

    override fun onResume() {
        super.onResume()
        setCountryToView(model.selectedCountry)
        setCityToView(model.selectedCity)
        setStartDateToView(model.startDate)
        setEndDateToView(model.endDate)
        setDescriptionToView(model.description)
    }

    private fun setDescriptionToView(description: String) {
        if (description.isNotEmpty()) et_description.setText(description)
    }

    private fun setEndDateToView(m: Long) {
        if (m != 0L) DateUtil.apply {
            tv_end_date_hint.visibility = View.VISIBLE
            tv_end_date.textColor = Color.BLACK
            tv_end_date.text = "${millisToFormattedDay(m)}.${millisToFormattedMonth(m)}.${millisToYear(m)}"
        }
    }

    private fun setStartDateToView(m: Long) {
        if (m != 0L) DateUtil.apply {
            tv_start_date_hint.visibility = View.VISIBLE
            tv_start_date.textColor = Color.BLACK
            tv_start_date.text = "${millisToFormattedDay(m)}.${millisToFormattedMonth(m)}.${millisToYear(m)}"
        }
    }

    private fun setCityToView(city: City) {
        if (city.id != -1) {
            tv_hint_city.visibility = View.VISIBLE
            tv_city.textColor = Color.BLACK
            tv_city.text = city.name
        }
    }

    private fun setCountryToView(selectedCountry: CountryModel) {
        if (selectedCountry.id != -1) {
            tv_hint_country.visibility = View.VISIBLE
            tv_country.textColor = Color.BLACK
            tv_country.text = selectedCountry.name
        }
    }

    private fun setupUi() {
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        btn_next.setOnClickListener {
            AppFragmentManager.openFragment(activity!!, CreateEventThirdFragment())
        }

        val startDateFragment =
            StartDateFragment()
        val endDateFragment =
            EndDateFragment()

        et_description.addTextChangedListener(object : AppTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                model.description = s.toString().trim()
            }
        })

        startDateFragment.onDateSelected = {
            when {
                model.endDate == 0L -> model.startDate = it
                model.endDate > it -> model.startDate = it
                else -> toast("Дата начала не должна быть больше даты окончания")
            }
        }
        endDateFragment.onDateSelected = {
            if (model.startDate < it) model.endDate = it
            else toast("Дата окончания должна быть больше даты начала")
        }

        wrp_country.setOnClickListener {
            DialogBuilder.showCountriesDialog(context!!, model.getCountryList()) {
                setCountryToView(it)
                model.selectedCountry = (it)
                model.getCityListInCounty()
            }
        }

        wrp_city.setOnClickListener {
            if (model.selectedCountry.id != -1) {
                DialogBuilder.showCitiesDialog(context!!, model.getCityList()) {
                    setCityToView(it)
                    model.selectedCity = it
                }
            } else toast("Сначала выберите страну")
        }

        wrp_start_date.setOnClickListener {
            AppFragmentManager.openFragment(activity!!, startDateFragment)
        }
        wrp_end_date.setOnClickListener {
            AppFragmentManager.openFragment(activity!!, endDateFragment)
        }

        TextInputUtil.setObservedEtEnteredLenghtToTV(et_description, tv_description_lenght)
    }
}