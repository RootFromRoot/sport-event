package com.app.sportevent.ui.filter.ui.country

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.app.sportevent.R
import com.app.sportevent.data.poko.CountryModel
import com.app.sportevent.data.poko.SportKindModel
import com.app.sportevent.data.poko.TypeEventModel
import kotlinx.android.synthetic.main.list_item_base_string.view.*

class CountryListAdapter : RecyclerView.Adapter<Holder>(), Filterable {

    private var items: ArrayList<CountryModel> = ArrayList()
    var onItemCheck: (eventType: CountryModel, isChecked: Boolean) -> Unit = { _, _ -> }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        Holder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.list_item_base_string, parent, false)
        )

    override fun getItemCount() = items.size

    fun setItems(items: ArrayList<CountryModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item = items[position]

        holder.itemView.apply {
            tv_sport_title.text = item.name
            selector.isChecked = item.isSelected
            this.setOnClickListener {
                selector.isChecked = !selector.isChecked
            }
            selector.setOnCheckedChangeListener { btn, isChecked ->
                onItemCheck(item, isChecked)
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            private var filterResultList: ArrayList<CountryModel> = ArrayList()
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val results = FilterResults()
                if (constraint!!.isNotEmpty()) {
                    items.forEach {
                        if (it.name.toLowerCase().contains(constraint.toString()))
                            filterResultList.add(it)
                    }
                    results.values = filterResultList
                    results.count = filterResultList.size
                }
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results?.count == 0) this@CountryListAdapter.setItems(items)
                else this@CountryListAdapter.setItems(filterResultList)
                notifyDataSetChanged()
            }
        }
    }
}

class Holder(view: View) : RecyclerView.ViewHolder(view)