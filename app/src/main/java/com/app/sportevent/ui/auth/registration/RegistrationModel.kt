package com.app.sportevent.ui.auth.registration

import com.app.sportevent.data.NO_CONNECTION_TEXT
import com.app.sportevent.data.SERVER_RESPONSE_OK
import com.app.sportevent.data.poko.CountriesResponse
import com.app.sportevent.data.poko.request.RegistrationRequest
import com.app.sportevent.data.util.FirebaseProvider
import com.app.sportevent.data.util.PrefManager
import com.app.sportevent.ui.auth.base.AuthModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.ConnectException

class RegistrationModel(private val prefManager: PrefManager) : AuthModel(prefManager) {

    fun register(request: RegistrationRequest, onRegister: (isRegistered: Boolean) -> Unit) {
        FirebaseProvider.provideFireBaseToken { firebaseToken ->
            GlobalScope.launch {
                request.lang = "RU"
                request.device = 0
                request.device_token = firebaseToken
                val fieldMap: HashMap<String, Any?> = HashMap()
                fieldMap["pass"] = request.pass
                fieldMap["email"] = request.email
                fieldMap["name"] = request.name
                fieldMap["lang"] = request.lang
                fieldMap["country_id"] = request.country_id
                fieldMap["device_token"] = request.device_token
                fieldMap["device"] = request.device
                try {
                    val response = api.registrate(fieldMap).await()
                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            if (response.body()!!.status == SERVER_RESPONSE_OK) {
                                prefManager.saveToken(response.body()!!.token)
                                onRegister(true)
                            } else onError(response.body()!!.comment + "")
                        } else onRegister(false)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } catch (e: ConnectException) {
                    onError(NO_CONNECTION_TEXT)
                }
            }
        }
    }


    fun getCountryList(onFetched: (list: CountriesResponse) -> Unit) {
        GlobalScope.launch {
            try {
                val response = api.getCountriesList("RU").await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            onFetched(response.body()!!)
                        }
                    else withContext(Dispatchers.Main) {
                        onError(response.body()!!.comment)
                    }
                }
            } catch (e: ConnectException){
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

}