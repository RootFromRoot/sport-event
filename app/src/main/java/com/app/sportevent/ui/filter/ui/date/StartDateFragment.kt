package com.app.sportevent.ui.filter.ui.date

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.sportevent.R
import com.app.sportevent.data.util.DateUtil
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.create.CreateEventModel
import com.app.sportevent.ui.filter.FilterModel
import kotlinx.android.synthetic.main.fragment_filter_single_date.*
import org.koin.android.ext.android.inject

class StartDateFragment : BaseFragment() {
    private val model: FilterModel by inject()
    var onDateSelected: (date: Long) -> Unit = {}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_filter_single_date, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
        setupUi()
    }

    private fun setupUi() {
        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }

        calendar_view.setOnDateChangeListener { _, i, i2, i3 ->
            val date = DateUtil.millisecondsFromCalendarView(i,i2,i3)
            model.startDate =(date)
            onDateSelected(date)
        }
        btn_reset_filter.setOnClickListener { calendar_view.date = System.currentTimeMillis() }
    }

    private fun initViews() {
        if (model.startDate != 0L)
            calendar_view.date = model.startDate
    }

}