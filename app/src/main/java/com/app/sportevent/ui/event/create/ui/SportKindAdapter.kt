package com.app.sportevent.ui.event.create.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.app.sportevent.R
import com.app.sportevent.data.BASE_URL
import com.app.sportevent.data.ENDPOINT_ICON
import com.app.sportevent.data.poko.SportKindModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_sport_kind.view.*
import timber.log.Timber


class SportKindAdapter : RecyclerView.Adapter<Holder>(), Filterable {

    private var items: ArrayList<SportKindModel> = ArrayList()
    var onItemCheck: (sportKind: SportKindModel) -> Unit = { }
    var onNothingFound: () -> Unit = {}

    private var lastCheckedPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        Holder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.list_item_sport_kind, parent, false)
        )

    override fun getItemCount() = items.size

    fun setItems(items: ArrayList<SportKindModel>) {
        this.items = items
        val checkedKind = items.find { it.isSelected }
        lastCheckedPosition = if (checkedKind != null) items.indexOf(checkedKind)
        else -1
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item = items[position]

        holder.itemView.apply {
            tv_sport_title.text = item.name

            val url = BASE_URL + ENDPOINT_ICON + item.icon
            Picasso
                .get()
                .load(url)
                .into(ic, object : Callback {
                    override fun onError(e: Exception?) {
                        e?.printStackTrace()
                    }

                    override fun onSuccess() {
                        Timber.i("")
                    }
                })
            selector.isChecked = position == lastCheckedPosition
            this.setOnClickListener { performClick(position, item) }
            selector.setOnClickListener { performClick(position, item) }
        }
    }

    private fun performClick(position: Int, item: SportKindModel) {
        val copyOfLastCheckedPosition = lastCheckedPosition
        lastCheckedPosition = position
        notifyItemChanged(copyOfLastCheckedPosition)
        notifyItemChanged(lastCheckedPosition)

        onItemCheck(item)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            private var filterResultList: ArrayList<SportKindModel> = ArrayList()
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val results = FilterResults()
                if (constraint!!.isNotEmpty()) {
                    items.forEach {
                        if (it.name.toLowerCase().contains(constraint.toString()))
                            filterResultList.add(it)
                    }
                    results.values = filterResultList
                    results.count = filterResultList.size
                }
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results?.count == 0) setItems(items)
                else setItems(filterResultList)
                notifyDataSetChanged()
            }
        }
    }
}

class Holder(view: View) : RecyclerView.ViewHolder(view)