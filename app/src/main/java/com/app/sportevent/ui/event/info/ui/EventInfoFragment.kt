package com.app.sportevent.ui.event.info.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.sportevent.R
import com.app.sportevent.data.poko.Event
import com.app.sportevent.data.poko.UserResponse
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.data.util.DialogBuilder
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.edit.ui.EditEventFirstFragment
import com.app.sportevent.ui.event.info.EventModel
import kotlinx.android.synthetic.main.fragment_event_info.*
import org.koin.android.ext.android.inject

class EventInfoFragment : BaseFragment() {

    private val model: EventModel by inject()
    var eventId = -1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_event_info, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
        model.currentEventId = eventId

        model.getCurrentEvent { event ->
            model.getCurrentUser { user ->
                initBtn(user, event)
            }
            initView()
        }
    }

    private fun setupUi() {
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
    }

    private fun initBtn(user: UserResponse, event: Event) {
        if (user.user.id == event.user.id) {
            initSelfEventBtn()
        } else {
            initOtherEventBtn()
        }
    }

    private fun initOtherEventBtn() {
        wrp_other_event.visibility = View.VISIBLE
        wrp_self_event.visibility = View.GONE

        btn_write.setOnClickListener {

        }

        btn_participation.setOnClickListener {

        }
    }

    private fun initSelfEventBtn() {
        wrp_other_event.visibility = View.GONE
        wrp_self_event.visibility = View.VISIBLE

        btn_delete.setOnClickListener {
            DialogBuilder.showDeleteDialog(model.title, this) {
                model.deleteEvent {
                    AppFragmentManager.closeFragment(activity!!)
                }
            }
        }
        btn_edit.setOnClickListener {
            AppFragmentManager.openFragment(
                activity!!,
                EditEventFirstFragment(model.currentEventId)
            )
        }
    }

    private fun initView() {
        toolbarTitle.text = if (model.title.isNotEmpty()) model.title else "Ивент"
        view_pager.adapter = EventPagerAdapter(activity!!.supportFragmentManager)
        tab_layout.setupWithViewPager(view_pager)
    }
}

