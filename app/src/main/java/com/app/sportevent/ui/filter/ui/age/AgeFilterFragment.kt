package com.app.sportevent.ui.filter.ui.age

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.sportevent.R
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.filter.FilterModel
import kotlinx.android.synthetic.main.fragment_filter_age.*
import kotlinx.android.synthetic.main.fragment_filter_age.seekbar_age
import org.koin.android.ext.android.inject

class AgeFilterFragment : BaseFragment() {

    private val model: FilterModel by inject()
    var onAgeRangeSelected: (min: Int, max: Int) -> Unit = { _, _ -> }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_filter_age, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
        setupUi()
    }

    private fun initViews() {
        seekbar_age.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            tv_start_value.text = minValue.toString()
            tv_end_value.text = maxValue.toString()
            model.ageMax = maxValue.toInt()
            model.ageMin = minValue.toInt()
        }

        seekbar_age.setMinValue(model.ageMin.toFloat())
        seekbar_age.setMaxValue(model.ageMax.toFloat())
        tv_start_value.text = model.ageMin.toString()
        tv_end_value.text = model.ageMax.toString()
    }

    private fun setupUi() {
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_ios_24px)
        toolbar.setNavigationOnClickListener {
            onAgeRangeSelected(seekbar_age.left, seekbar_age.right)
            AppFragmentManager.closeFragment(activity!!)
        }
        btn_reset_filter.setOnClickListener {
            seekbar_age.setMinValue( 6f)
            seekbar_age.setMinValue (80f)
            model.ageMax = (0)
            model.ageMin = (0)
        }
    }

}