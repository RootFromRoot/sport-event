package com.app.sportevent.ui.filter.ui.type.event

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.sportevent.R
import com.app.sportevent.data.poko.TypeEventModel
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.filter.FilterModel
import kotlinx.android.synthetic.main.fragment_filter_base.*
import org.koin.android.ext.android.inject

class EventTypeFragment : BaseFragment() {

    private val model: FilterModel by inject()
    private val adapter = EventTypeListAdapter()
    private var eventTypeList: ArrayList<TypeEventModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_filter_base, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
        initViews()
    }

    private fun initViews() {
            eventTypeList = if (model.eventTypeList.isEmpty())
                 model.filterData.typeEvent
             else model.eventTypeList

        adapter.setItems(eventTypeList)
    }

    private fun setupUi() {
        iv_back.setOnClickListener {  AppFragmentManager.closeFragment(activity!!) }
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        tv_toolbar_title.text = "Тип ивента"

        btn_reset_filter.setOnClickListener {
            eventTypeList.forEach { it.isSelected = false }
            model.eventTypeList = eventTypeList
            adapter.setItems(eventTypeList)
            model.filter {  }
        }

        rv_sport_list.layoutManager = LinearLayoutManager(activity!!)
        rv_sport_list.adapter = adapter

        adapter.onItemCheck = { type, isChecked ->
            eventTypeList.forEach { if (it.id == type.id) it.isSelected = isChecked }
            model.eventTypeList = eventTypeList
            model.filter{}
        }
    }

}
