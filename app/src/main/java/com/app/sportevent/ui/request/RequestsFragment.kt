package com.app.sportevent.ui.request

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.sportevent.R
import com.app.sportevent.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_my_profile.*

class RequestsFragment: BaseFragment(){
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_requests, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
    }

    private fun setupUi() {
        activity().onBackClick = {}
    }
}