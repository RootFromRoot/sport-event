package com.app.sportevent.ui.event.info.ui.page

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.sportevent.R
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.info.EventModel
import org.koin.android.ext.android.inject

class EventPreviewMembersFragment : BaseFragment() {

    private val model: EventModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_event_preview_members, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
    }

    private fun setupUi() {
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
    }
}