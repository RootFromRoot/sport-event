package com.app.sportevent.ui.auth.restore

import com.app.sportevent.data.Api
import com.app.sportevent.data.NO_CONNECTION_TEXT
import com.app.sportevent.data.SERVER_RESPONSE_OK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.ConnectException

class RestorePasswordModel {

    var onError: (message: String) -> Unit = {}

    fun restore(email: String, onSuccess: () -> Unit) {
        GlobalScope.launch {
            try {
                val response = Api.get().restorePassword(email).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) { onSuccess() }
                    else
                        withContext(Dispatchers.Main) { onError(response.body()!!.comment) }
                }
            }catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }
}