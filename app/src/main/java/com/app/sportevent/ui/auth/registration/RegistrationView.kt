package com.app.sportevent.ui.auth.registration

import com.app.sportevent.data.poko.CityResponse
import com.app.sportevent.data.poko.CountriesResponse
import com.app.sportevent.ui.auth.base.AuthView

interface RegistrationView: AuthView {
    fun setCountryListToView(list: CountriesResponse)
}