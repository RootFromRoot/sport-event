package com.app.sportevent.ui.event.info

import android.content.Context
import com.app.sportevent.data.Api
import com.app.sportevent.data.NO_CONNECTION_TEXT
import com.app.sportevent.data.SERVER_RESPONSE_OK
import com.app.sportevent.data.poko.*
import com.app.sportevent.data.util.PrefManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.ConnectException

class EventModel(context: Context) {

    private val prefManager = PrefManager(context)
    var onError: (message: String?) -> Unit = {}

    var title = ""
    var sportKind: SportKindModel = SportKindModel(0, "", "")
    var eventType = TypeEventModel(0, "", false)
    var memberType = TypeParticipantModel(0, 0, 0, "", false)
    var ageMax = 0
    var ageMin = 0

    var country = CountryModel(-1, "", "")
    var city = City(-1, "", 0.0, 0.0, "")
    var startDate = 0L
    var endDate = 0L
    var description = ""

    var memberCount = 0
    var memberPayment = ""
    var includedToPayment = ""
    var prizePool = ""

    var imagePath: String? = null

    var sexList = arrayListOf(
        Sex(1, "Мужской", false),
        Sex(2, "Женский", false)
    )

    var currentEventId = -1
    var currentUser = User()
    var eventUser = User()

    fun getCurrentUser(onFetched: (user: UserResponse) -> Unit) {
        GlobalScope.launch {
            try {
                val response = Api.get().getUserInfo(prefManager.getToken()).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            val user = response.body()!!
                            currentUser = user.user
                            onFetched(user)
                        }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    fun getCurrentEvent(onFetched: (event: Event) -> Unit) {
        GlobalScope.launch {
            try {
                val response = Api.get().getEventList(prefManager.getToken()).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            val events = response.body()!!.events
                            val event = events.find { it.id == currentEventId }!!
                            eventUser = event.user
                            setVariables(event)
                            onFetched(event)
                        }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    fun deleteEvent(onDeleted: () -> Unit) {
        GlobalScope.launch {
            try {
                val response = Api.get().deleteEvent(prefManager.getToken(), currentEventId).await()
                if (response.isSuccessful) {
                    if (response.body()!!.status == SERVER_RESPONSE_OK)
                        withContext(Dispatchers.Main) {
                            onDeleted()
                        }
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    private fun setVariables(event: Event) {
        event.apply {
            title = name + ""
            this@EventModel.city = city!!
            this@EventModel.country = country!!
            this@EventModel.eventType = eventType!!
            this@EventModel.memberType = memberType!!
            this@EventModel.sportKind = sportKind!!
            initSexList(gender)
            imagePath = icon
            ageMax = age_max!!.toInt()
            ageMin = age_min!!.toInt()
            startDate = date_start!! * 1000
            endDate = date_stop!! * 1000
            this@EventModel.description = description!!
            memberCount = participant_count!!.toInt()
            memberType = this@EventModel.memberType
            prizePool = prize_pool!!
            includedToPayment = event.participant_sum.toString()
            currentUser = User()
        }
    }

    private fun initSexList(gender: Int?) {
        when (gender) {
            0 -> {
                sexList[0].isChecked = true
                sexList[1].isChecked = true
            }
            1 -> sexList[0].isChecked = true
            2 -> sexList[1].isChecked = true
        }
    }
}