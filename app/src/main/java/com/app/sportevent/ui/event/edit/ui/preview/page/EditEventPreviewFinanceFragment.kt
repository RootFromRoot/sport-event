package com.app.sportevent.ui.event.edit.ui.preview.page

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.sportevent.R
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.edit.EditEventModel
import kotlinx.android.synthetic.main.fragment_event_preview_finance.*
import org.koin.android.ext.android.inject

class EditEventPreviewFinanceFragment : BaseFragment() {

    private val model: EditEventModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_event_preview_finance, container, false)!!

    override fun onResume() {
        super.onResume()
        initView()
    }

    private fun initView() {
        tv_member_payment.text = model.event.participant_sum.toString()
        tv_included_to_payment.text = model.event.participant_ante
        tv_prize_pool.text = model.event.prize_pool
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
    }
}