package com.app.sportevent.ui.auth.registration

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.app.sportevent.R
import com.app.sportevent.data.RC_SIGN_IN
import com.app.sportevent.data.poko.CityResponse
import com.app.sportevent.data.poko.CountriesResponse
import com.app.sportevent.data.poko.CountryModel
import com.app.sportevent.data.toArray
import com.app.sportevent.data.util.KeyboardUtils
import com.app.sportevent.data.util.LocationManager
import com.app.sportevent.data.util.PermissionManager
import com.app.sportevent.data.util.PermissionRequester
import com.app.sportevent.data.util.builder.RegistrationRequestBuilder
import com.app.sportevent.ui.MainActivity
import com.app.sportevent.ui.auth.base.SocialNetworkAuthManager
import com.facebook.login.LoginResult
import com.vk.api.sdk.auth.VKAccessToken
import kotlinx.android.synthetic.main.activity_registration.*
import org.koin.android.ext.android.inject

class RegistrationActivity : AppCompatActivity(), RegistrationView {

    private val presenter: RegistrationPresenter by inject()
    private lateinit var progressDialog: ProgressDialog
    private var selectedCountryId = -1
    private var countryList: ArrayList<CountryModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        presenter.bind(this)
        presenter.bindAuthView(this)
        SocialNetworkAuthManager.init(this)
        setupUi()
        checkLocationPermission()
        getLocation()
    }

    private fun getLocation() {
        if (PermissionManager.checkIsLocationPermissionGranted(this))
            LocationManager.getLastLocation(this) {
                presenter.getCountryByLocation(it.latitude, it.longitude)
            }
        presenter.setCountryListToView()
    }

    override fun setCountryListToView(list: CountriesResponse) {
        countryList = list.countries
    }

    private fun showCountriesDialog(list: List<CountryModel>) {
        val titleList = arrayListOf<String>()
        for (i in list.indices) {
            titleList.add(list[i].name)
        }
        val items = toArray<String>(titleList)
        val builder = AlertDialog.Builder(this)
        with(builder) {
            setTitle("Выберите страну")
            setItems(items) { dialog, which ->
                selectedCountryId = list[which].id
                countryField.setText(list[which].name)
            }

            setNegativeButton("Отмена") { dialog, which ->
                dialog.dismiss()
            }
            show()
        }
    }

    private fun checkLocationPermission() {
        if (!PermissionManager.checkIsLocationPermissionGranted(this))
            PermissionRequester.requestLocationPermission(this)
    }

    override fun setCityToView(city: CityResponse) {
        countryField.setText(city.country.name)
        selectedCountryId = city.country.id
        countryField.isEnabled = false
    }

    private fun setupUi() {
        progressDialog = ProgressDialog(this)
        iv_back.setOnClickListener { onBackPressed() }
        fbBtn.setOnClickListener {
            SocialNetworkAuthManager.requestAuthWithFb {
                registerWithFb(it)
            }
        }
        vkBtn.setOnClickListener { SocialNetworkAuthManager.requestAuthWithVk() }
        gpBtn.setOnClickListener { SocialNetworkAuthManager.requestAuthWithGoogle() }
        signUpBtn.setOnClickListener {
            if (checkIsFieldsValid()) {
                KeyboardUtils.forceCloseKeyboard(this)

                progressDialog.setMessage("Регистрация пользователя...")
                progressDialog.show()

                val registrationRequest = RegistrationRequestBuilder.buildeEmpty()
                registrationRequest.pass = passwordField.text.toString()
                registrationRequest.name = nameField.text.toString()
                registrationRequest.email = emailField.text.toString()
                presenter.userWantRegister(registrationRequest)
            }
        }

        countryField.setOnClickListener { showCountriesDialog(countryList) }
    }

    private fun registerWithVk(token: VKAccessToken) {
        if (selectedCountryId != -1) {
            progressDialog.setMessage("Регистрация пользователя...")
            progressDialog.show()
            presenter.userWantAuthWithVk(token, selectedCountryId)
        } else toast("Выберите страну")
    }

    override fun showVkNoEmailToast() {
        progressDialog.dismiss()
        toast("В Вашем профиле отсутствует e-mail. Воспользуйтесь другими вариантами регистрации!")
    }

    private fun registerWithGoogle(uid: String) {
        if (selectedCountryId != -1) {
            progressDialog.setMessage("Регистрация пользователя...")
            progressDialog.show()
            presenter.userWantAuthWithGoogle(uid, selectedCountryId)
        } else toast("Выберите страну")
    }

    private fun registerWithFb(loginResult: LoginResult) {
        if (selectedCountryId != -1) {
            progressDialog.setMessage("Регистрация пользователя...")
            progressDialog.show()
            presenter.userWantAuthWithFb(loginResult, selectedCountryId)
        } else toast("Выберите страну")
    }

    private fun checkIsFieldsValid(): Boolean {
        return if (nameField.text.isNullOrEmpty()) {
            toast("Введите имя")
            false
        } else if (emailField.text.isNullOrEmpty()) {
            toast("Введите e-mail")
            false
        } else if (!emailField.text.toString().contains("@")) {
            toast("Некорректный адрес электронной почты")
            false
        } else if (passwordField.text.isNullOrEmpty()) {
            toast("Введите пароль")
            false
        } else if (countryField.text.isNullOrEmpty()) {
            toast("Выберите страну")
            false
        } else return true
    }

    private fun toast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    override fun closeProgressDialog() {
        progressDialog.dismiss()
    }

    override fun showPostListScreen() {
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun showAuthErrorMsg() {
        toast("Ошибка регистрации")
    }

    override fun showErrorToast(text: String) {
        closeProgressDialog()
        toast(text)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        SocialNetworkAuthManager.notifyActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RC_SIGN_IN -> {
                SocialNetworkAuthManager.handleGoogleAuthResponse(data) {
                    registerWithGoogle(it)
                }
            }
            else -> {
                if (!SocialNetworkAuthManager.handleVkAuthResponse(
                        requestCode,
                        resultCode,
                        data
                    ) { registerWithVk(it) }
                ) {
                    super.onActivityResult(requestCode, resultCode, data)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        /*   if (requestCode == RC_PERMISSION_LOCATION) {
               if (grantResults[0] == PackageManager.PERMISSION_GRANTED) toast("")
           }*/
    }
}
