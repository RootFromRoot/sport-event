package com.app.sportevent.ui.auth.login

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.sportevent.R
import com.app.sportevent.data.RC_SIGN_IN
import com.app.sportevent.data.poko.CityResponse
import com.app.sportevent.data.util.KeyboardUtils
import com.app.sportevent.data.util.LocationManager
import com.app.sportevent.data.util.PermissionManager
import com.app.sportevent.ui.MainActivity
import com.app.sportevent.ui.auth.base.SocialNetworkAuthManager
import com.app.sportevent.ui.auth.restore.ResetPasswordActivity
import com.facebook.login.LoginResult
import com.vk.api.sdk.auth.VKAccessToken
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject

class LoginActivity : AppCompatActivity(), LoginView {

    private val presenter: LoginPresenter by inject()
    private lateinit var progressDialog: ProgressDialog

    private var selectedCountryId = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog = ProgressDialog(this)
        setContentView(R.layout.activity_login)
        SocialNetworkAuthManager.init(this)
        presenter.bind(this)
        presenter.bindAuthView(this)
        setupUi()
        getLocation()
    }

    private fun getLocation() {
        if (PermissionManager.checkIsLocationPermissionGranted(this))
            LocationManager.getLastLocation(this) {
                presenter.getCountryByLocation(it.latitude, it.longitude)
            }
        else {
            selectedCountryId = 1
        }
    }

    override fun setCityToView(city: CityResponse) {
        selectedCountryId = city.country.id
    }

    private fun setupUi() {
        iv_back.setOnClickListener { onBackPressed() }
        fbBtn.setOnClickListener {
            SocialNetworkAuthManager.requestAuthWithFb {
                loginWithFb(it)
            }
        }
        vkBtn.setOnClickListener { SocialNetworkAuthManager.requestAuthWithVk() }
        gpBtn.setOnClickListener { SocialNetworkAuthManager.requestAuthWithGoogle() }
        signInBtn.setOnClickListener { login() }
        textForgot.setOnClickListener { showRestorePasswordScreen() }
    }

    private fun showRestorePasswordScreen() {
        startActivity(Intent(this, ResetPasswordActivity::class.java))
    }

    private fun login() {
        KeyboardUtils.forceCloseKeyboard(this)
        if (isEtValid()) {
            progressDialog.setMessage("Авторизация пользователя...")
            progressDialog.show()

            presenter.userWantLogin(
                emailField.text.toString().trim(),
                passwordField.text.toString().trim()
            )
        }
    }

    private fun isEtValid(): Boolean {
        return if (emailField.text.isNullOrEmpty()) {
            toast("Введите e-mail")
            false
        } else if (!emailField.text.toString().contains("@")) {
            toast("Некорректный адрес электронной почты")
            false
        } else if (passwordField.text.isNullOrEmpty()) {
            toast("Введите пароль")
            false
        } else true
    }

    override fun showVkNoEmailToast() {
        progressDialog.dismiss()
        toast("В Вашем профиле отсутствует e-mail. Воспользуйтесь другими вариантами регистрации!")
    }

    private fun loginWithFb(loginResult: LoginResult) {
        presenter.userWantAuthWithFb(loginResult, selectedCountryId)
    }

    private fun loginWithGoogle(uid: String) {
        presenter.userWantAuthWithGoogle(uid, selectedCountryId)
    }

    private fun loginWithVk(token: VKAccessToken) {
        presenter.userWantAuthWithVk(token, selectedCountryId)
    }

    override fun closeProgressDialog() {
        progressDialog.dismiss()
    }

    override fun showPostListScreen() {
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun showAuthErrorMsg() {
        toast("Ошибка авторизации")
    }

    override fun showErrorToast(text: String) {
        toast(text)
        closeProgressDialog()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        SocialNetworkAuthManager.notifyActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RC_SIGN_IN -> {
                SocialNetworkAuthManager.handleGoogleAuthResponse(data) {
                    loginWithGoogle(it)
                }
            }
            else -> {
                if (!SocialNetworkAuthManager.handleVkAuthResponse(
                        requestCode,
                        resultCode,
                        data
                    ) { loginWithVk(it) }
                ) {
                    super.onActivityResult(requestCode, resultCode, data)
                }
            }
        }
    }
}
