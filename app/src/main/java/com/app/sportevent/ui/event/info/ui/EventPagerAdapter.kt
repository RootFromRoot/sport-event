package com.app.sportevent.ui.event.info.ui

import android.os.Parcelable
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.app.sportevent.ui.event.info.ui.page.EventPreviewFeedbackFragment
import com.app.sportevent.ui.event.info.ui.page.EventPreviewFinanceFragment
import com.app.sportevent.ui.event.info.ui.page.EventPreviewGeneralFragment
import com.app.sportevent.ui.event.info.ui.page.EventPreviewMembersFragment

class EventPagerAdapter(fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount() = 4

    // need to reload pagerAdapter each time
    // https://stackoverflow.com/questions/7263291/viewpager-pageradapter-not-updating-the-view
    override fun saveState(): Parcelable? {
        return null
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {

    }

    override fun getItem(position: Int): Fragment {
        return when (position){
            0-> EventPreviewGeneralFragment()
            1-> EventPreviewMembersFragment()
            2-> EventPreviewFinanceFragment()
            3-> EventPreviewFeedbackFragment()
            else -> EventPreviewGeneralFragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position){
            0-> "Общее"
            1-> "Участники"
            2-> "Финансы"
            3-> "Отзывы"
            else -> "Общее"
        }
    }
}