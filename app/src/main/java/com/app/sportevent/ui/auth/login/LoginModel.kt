package com.app.sportevent.ui.auth.login

import com.app.sportevent.data.Api
import com.app.sportevent.data.NO_CONNECTION_TEXT
import com.app.sportevent.data.SERVER_RESPONSE_NOT_FOUND
import com.app.sportevent.data.SERVER_RESPONSE_OK
import com.app.sportevent.data.poko.request.LoginRequest
import com.app.sportevent.data.util.FirebaseProvider
import com.app.sportevent.data.util.PrefManager
import com.app.sportevent.ui.auth.base.AuthModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.ConnectException

class LoginModel(private val prefManager: PrefManager) : AuthModel(prefManager) {
    fun login(email: String, pass: String, onSuccess: () -> Unit) {
        FirebaseProvider.provideFireBaseToken { firebaseToken ->
            GlobalScope.launch {
                val request = LoginRequest(
                    device_token = firebaseToken,
                    device = 0,
                    email = email,
                    pass = pass
                )
                val fieldMap: HashMap<String, Any?> = HashMap()
                fieldMap["pass"] = request.pass
                fieldMap["device_token"] = request.device_token
                fieldMap["email"] = request.email
                fieldMap["device"] = request.device
                try {
                    val response = Api.get().login(fieldMap).await()
                    if (response.isSuccessful) {
                        when (response.body()!!.status) {
                            SERVER_RESPONSE_OK -> {
                                prefManager.saveToken(response.body()!!.token)
                                withContext(Dispatchers.Main) { onSuccess() }
                            }
                            SERVER_RESPONSE_NOT_FOUND ->
                                withContext(Dispatchers.Main) {
                                    onError("Пользователь не зарегестрироавн" + "")
                                }
                            else -> withContext(Dispatchers.Main) {
                                onError("Неверный логин или пароль")
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                catch (e: ConnectException) {
                    onError(NO_CONNECTION_TEXT)
                }
            }
        }
    }
}