package com.app.sportevent.ui.auth.registration

import com.app.sportevent.data.poko.request.RegistrationRequest
import com.app.sportevent.ui.auth.base.AuthPresenter
import com.app.sportevent.ui.auth.base.AuthView

class RegistrationPresenter(private val model: RegistrationModel): AuthPresenter(model) {

    private lateinit var view: RegistrationView

    fun bind(view: RegistrationView) {
        this.view = view
        model.onError = { view.showErrorToast("Error $it") }
    }

    fun userWantRegister(registrationRequest: RegistrationRequest) {
        model.register(registrationRequest) {
            if (it) view.showPostListScreen()
            else view.showAuthErrorMsg()
            view.closeProgressDialog()
        }
    }

    fun setCountryListToView() {
        model.getCountryList{
            view.setCountryListToView(it)
        }
    }
}