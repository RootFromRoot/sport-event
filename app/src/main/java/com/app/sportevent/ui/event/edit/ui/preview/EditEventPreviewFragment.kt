package com.app.sportevent.ui.event.edit.ui.preview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.sportevent.R
import com.app.sportevent.data.OPENED_FROM_EVENT_LIST
import com.app.sportevent.data.OPENED_FROM_MY_PROFILE
import com.app.sportevent.data.toast
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.ui.MainActivity
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.edit.EditEventModel
import com.app.sportevent.ui.list.ui.EventListFragment
import com.app.sportevent.ui.profile.UserProfileFragment
import kotlinx.android.synthetic.main.fragment_event_preview.*
import org.koin.android.ext.android.inject

class EditEventPreviewFragment : BaseFragment() {

    private val model: EditEventModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_event_preview, container, false)!!

    override fun onResume() {
        super.onResume()
        initView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
    }

    private fun setupUi() {
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        btn_edit.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        btn_create.setOnClickListener {
            model.updateEvent {
                if (it) {
                    toast("Ивент обновлен")
                    AppFragmentManager.clearBackStack(activity!!)
                    val fragment = when ((activity as MainActivity).openedFrom) {
                        OPENED_FROM_MY_PROFILE -> {
                            (activity as MainActivity).setSelectedMyProfileFragment()
                            UserProfileFragment()
                        }
                        OPENED_FROM_EVENT_LIST -> {
                            (activity as MainActivity).setSelectedListFragment()
                            EventListFragment()
                        }
                        else -> {
                            (activity as MainActivity).setSelectedListFragment()
                            EventListFragment()
                        }
                    }
                    AppFragmentManager.openFragment(activity!!, fragment)
                } else toast("Ошибка. Заполните все поля!")
            }
        }
    }

    private fun initView() {
        toolbarTitle.text = if (!model.event.name.isNullOrEmpty()) model.event.name
        else "Редактирование ивента"
        view_pager.adapter = EditEventPagerAdapter(activity!!.supportFragmentManager)
        tab_layout.setupWithViewPager(view_pager)
    }
}