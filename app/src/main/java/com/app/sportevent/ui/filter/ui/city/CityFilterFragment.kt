package com.app.sportevent.ui.filter.ui.city

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.sportevent.R
import com.app.sportevent.data.poko.City
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.data.util.AppTextWatcher
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.filter.FilterModel
import kotlinx.android.synthetic.main.fragment_filter_base.*
import org.koin.android.ext.android.inject

class CityFilterFragment : BaseFragment() {

    private val model: FilterModel by inject()
    private val adapter = CityListAdapter()
    private var cityList: ArrayList<City> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_filter_base, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()

        cityList = model.cityList

        adapter.setItems(cityList)
    }

    private fun setupUi() {
        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        tv_toolbar_title.text = "Город"

        rv_sport_list.layoutManager = LinearLayoutManager(activity!!)
        rv_sport_list.adapter = adapter

        btn_reset_filter.setOnClickListener {
            cityList.forEach { it.isSelected = false }
            model.cityList = cityList
            adapter.setItems(cityList)
            model.filter {  }
        }

        et_search.addTextChangedListener(object : AppTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null) adapter.filter.filter(s)
            }
        })

        adapter.onItemCheck = { kind, isChecked ->
            cityList.forEach { if (it.id == kind.id) it.isSelected = isChecked }
            model.cityList = cityList
            model.filter{}
        }
    }
}