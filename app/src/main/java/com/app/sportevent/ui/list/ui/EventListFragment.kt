package com.app.sportevent.ui.list.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.sportevent.R
import com.app.sportevent.data.OPENED_FROM_EVENT_LIST
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.ui.MainActivity
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.create.ui.CreateEventFirstFragment
import com.app.sportevent.ui.event.info.ui.EventInfoFragment
import com.app.sportevent.ui.filter.ui.FilterEventsFragment
import com.app.sportevent.ui.list.EventListModel
import kotlinx.android.synthetic.main.fragment_events.*
import org.koin.android.ext.android.inject

class EventListFragment : BaseFragment() {

    private val model: EventListModel by inject()
    private val adapter = EventListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_events, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
    }

    override fun onResume() {
        super.onResume()
        model.getEventList {
            adapter.setItems(it)
        }
    }

    private fun setupUi() {
        rv_event_list.layoutManager = LinearLayoutManager(activity!!)
        rv_event_list.adapter = adapter

        adapter.onItemClick = {
            AppFragmentManager.openFragment(activity!!, EventInfoFragment().apply { eventId = it })
        }

        activity().onBackClick = {}
        fab_create_event.setOnClickListener {
            (activity as MainActivity).openedFrom = OPENED_FROM_EVENT_LIST
            AppFragmentManager.openFragment(activity!!, CreateEventFirstFragment())
        }
        iv_filter.setOnClickListener {
            AppFragmentManager.openFragment(activity!!, FilterEventsFragment())
        }
    }
}