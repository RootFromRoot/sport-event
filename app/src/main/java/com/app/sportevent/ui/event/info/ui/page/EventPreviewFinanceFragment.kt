package com.app.sportevent.ui.event.info.ui.page

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.sportevent.R
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.info.EventModel
import kotlinx.android.synthetic.main.fragment_event_preview_finance.*
import org.koin.android.ext.android.inject

class EventPreviewFinanceFragment : BaseFragment() {

    private val model: EventModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_event_preview_finance, container, false)!!

    override fun onResume() {
        super.onResume()
        initView()
    }

    private fun initView() {
        tv_member_payment.text = model.memberPayment
        tv_included_to_payment.text = model.includedToPayment
        tv_prize_pool.text = model.prizePool
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
    }
}