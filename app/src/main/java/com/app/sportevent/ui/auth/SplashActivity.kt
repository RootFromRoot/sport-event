package com.app.sportevent.ui.auth

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.sportevent.R
import com.app.sportevent.data.VALUE_NOT_FOUND
import com.app.sportevent.data.util.PrefManager
import com.app.sportevent.ui.MainActivity
import com.app.sportevent.ui.auth.login.LoginActivity
import com.app.sportevent.ui.auth.registration.RegistrationActivity
import kotlinx.android.synthetic.main.activity_splash.*
import org.koin.android.ext.android.inject

class SplashActivity : AppCompatActivity() {

    private val prefManager: PrefManager by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if (isUserLogin()) startActivity(Intent(this, MainActivity::class.java))

        signInBtn.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
        signUpBnt.setOnClickListener {
            startActivity(Intent(this, RegistrationActivity::class.java))
        }
    }

    private fun isUserLogin() = prefManager.getToken() != VALUE_NOT_FOUND
}
