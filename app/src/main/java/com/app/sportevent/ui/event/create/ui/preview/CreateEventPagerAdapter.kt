package com.app.sportevent.ui.event.create.ui.preview

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.app.sportevent.ui.event.create.ui.preview.page.CreateEventPreviewFeedbackFragment
import com.app.sportevent.ui.event.create.ui.preview.page.CreateEventPreviewFinanceFragment
import com.app.sportevent.ui.event.create.ui.preview.page.CreateEventPreviewGeneralFragment
import com.app.sportevent.ui.event.create.ui.preview.page.CreateEventPreviewMembersFragment

class CreateEventPagerAdapter(fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount() = 4

    override fun getItem(position: Int): Fragment {
        return when (position){
            0-> CreateEventPreviewGeneralFragment()
            1-> CreateEventPreviewMembersFragment()
            2-> CreateEventPreviewFinanceFragment()
            3-> CreateEventPreviewFeedbackFragment()
            else -> CreateEventPreviewGeneralFragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position){
            0-> "Общее"
            1-> "Участники"
            2-> "Финансы"
            3-> "Отзывы"
            else -> "Общее"
        }
    }
}