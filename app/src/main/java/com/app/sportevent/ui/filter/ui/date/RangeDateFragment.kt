package com.app.sportevent.ui.filter.ui.date

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.sportevent.R
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.data.util.DateUtil
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.filter.FilterModel
import kotlinx.android.synthetic.main.fragment_filter_range_date.*
import org.koin.android.ext.android.inject

class RangeDateFragment : BaseFragment() {
    private val model: FilterModel by inject()
    var onDateSelected: (date: Long) -> Unit = {}
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_filter_range_date, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
        setupUi()
    }

    private fun setupUi() {
        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        btn_reset_filter.setOnClickListener {
            calendar_view.clearSelection()
            model.selectedDates = listOf()
            model.startDate = 0L
            model.endDate = 0L
            model.filter {  }
        }

        calendar_view.setOnRangeSelectedListener { widget, dates ->
            model.selectedDates = dates

            model.startDate =
                DateUtil.millisecondsFromCalendarView(dates[0].year, dates[0].month, dates[0].day)
            dates.forEachIndexed { index, date ->
                if (index == dates.size - 1) {
                    model.endDate =
                        DateUtil.millisecondsFromCalendarView(date.year, date.month, date.day)
                }
            }
            model.filter{}
        }
    }

    private fun initViews() {
        if (model.startDate != 0L)
            model.selectedDates.forEach { calendar_view.setDateSelected(it, true) }
    }

}