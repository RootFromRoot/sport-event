package com.app.sportevent.ui.event.edit.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.sportevent.R
import com.app.sportevent.data.*
import com.app.sportevent.data.poko.Sex
import com.app.sportevent.data.poko.SportKindModel
import com.app.sportevent.data.poko.TypeEventModel
import com.app.sportevent.data.poko.TypeParticipantModel
import com.app.sportevent.data.util.*
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.create.ui.filter.SportKindFragment
import com.app.sportevent.ui.event.edit.EditEventModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_create_event_first.*
import kotlinx.android.synthetic.main.fragment_create_event_first.tv_event_type
import kotlinx.android.synthetic.main.fragment_create_event_first.tv_sex
import org.jetbrains.anko.textColor
import org.koin.android.ext.android.inject
import timber.log.Timber
import java.io.File

class EditEventFirstFragment(private val eventId: Int) : BaseFragment() {

    private val model: EditEventModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_create_event_first, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        model.initFilterData { }
        model.initCountryList()
        setupUi()
    }

    override fun onResume() {
        super.onResume()
        if (model.event.id == -1) {
            model.getCurrentEvent(eventId) { initView() }
        } else {
            initView()
        }
    }

    private fun initView() {
        setTitleToView(model.event.name.toString())
        setSportKindToView(model.event.sportKind)
        setEventTypeListToView(model.event.eventType)
        setMemberTypeListToView(model.event.memberType)
        setAgeRangeToView(model.event.age_min, model.event.age_max)
        setSexToView(model.sexList)
        setImageToView()
    }

    private fun setImageToView() {
        if (model.imagePath == null) {
            setEventImage(model.event.icon)
        } else {
            setLocalImage(model.imagePath!!)
        }
    }

    private fun setEventImage(icon: String?) {
        val url = BASE_URL + ENDPOINT_IMAGES + icon
        Picasso
            .get()
            .load(url)
            .resize(0, 400)
            .into(iv_image, object : Callback {
                override fun onError(e: Exception?) {
                    Timber.e(e, "error")
                }

                override fun onSuccess() {
                    Timber.i("success")
                }
            })
    }

    private fun setLocalImage(imagePath: String) {
        Picasso
            .get()
            .load(File(imagePath))
            .resize(0, 400)
            .into(iv_image, object : Callback {
                override fun onError(e: Exception?) {
                    Timber.e(e, "error")
                }

                override fun onSuccess() {
                    Timber.i("success")
                }
            })
    }

    private fun setSexToView(list: ArrayList<Sex>) {
        var text = ""
        val selectedSexList = list.filter { it.isChecked }
        if (selectedSexList.size == 1) text = selectedSexList[0].title
        else selectedSexList.forEachIndexed { index, sex ->
            val title = if (index != list.size - 1) "${sex.title}," else sex.title
            if (sex.isChecked) text = "$text $title"
        }
        if (text.isNotEmpty()) {
            tv_sex_hint.visibility = View.VISIBLE
            tv_sex.textColor = Color.BLACK
            tv_sex.text = text
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setAgeRangeToView(ageMin: Int?, ageMax: Int?) {
        if (ageMin != 0 && ageMax != 0) {
            tv_age_filter_hind.visibility = View.VISIBLE
            tv_age_filter.text = "$ageMin - $ageMax"
            tv_age_filter.textColor = Color.BLACK
        }
    }

    private fun setMemberTypeListToView(memberType: TypeParticipantModel?) {
        if (memberType?.id != 0) {
            tv_member_type_kind.visibility = View.VISIBLE
            tv_member_type.textColor = Color.BLACK
            tv_member_type.text = memberType?.name
        }
    }

    private fun setEventTypeListToView(eventType: TypeEventModel?) {
        if (eventType?.id != 0) {
            tv_event_type_hint.visibility = View.VISIBLE
            tv_event_type.textColor = Color.BLACK
            tv_event_type.text = eventType?.name
        }
    }

    private fun setTitleToView(title: String) {
        if (title.isNotEmpty()) et_title.setText(title)
    }

    private fun setSportKindToView(sportKindModel: SportKindModel?) {
        if (sportKindModel?.id != 0) {
            tv_hint_sport_kind.visibility = View.VISIBLE
            tv_sport_kind.textColor = Color.BLACK
            tv_sport_kind.text = sportKindModel?.name
        }
    }

    private fun setupUi() {
        btn_next.setOnClickListener {
            AppFragmentManager.openFragment(activity!!, EditEventSecondFragment())
        }

        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }

        et_title.addTextChangedListener(object : AppTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                model.event.name = s.toString().trim()
            }
        })

        wrp_sport_kind.setOnClickListener {
            AppFragmentManager.openFragment(activity!!, SportKindFragment())
        }
        wrp_event_type.setOnClickListener {
            DialogBuilder.showEventTypeDialog(context!!, model.filterData.typeEvent) { list ->
                setEventTypeListToView(list)
                model.event.eventType = list
            }
        }
        wrp_member_type.setOnClickListener {
            DialogBuilder.showMemberTypeDialog(context!!, model.filterData.typeParticipant) {
                setMemberTypeListToView(it)
                model.event.memberType = it
                model.event.age_min = it.age_min
                model.event.age_max = it.age_max
                setAgeRangeToView(it.age_min, it.age_max)
            }
        }
        wrp_age_filter.setOnClickListener {
            if (model.event.memberType?.id != 0)
                DialogBuilder.showAgeDialog(context!!, model.event.memberType!!) { min, max ->
                    setAgeRangeToView(min, max)
                    model.event.age_min = min
                    model.event.age_max = max
                }
            else toast("Сначала выберите тип участников!")
        }
        wrp_sex.setOnClickListener {
            DialogBuilder.showSexDialog(context!!, model.sexList) { list ->
                setSexToView(list)
                model.sexList = list
            }
        }

        photoBtn.setOnClickListener { onUploadPhotoClick() }
    }

    private fun onUploadPhotoClick() {
        if (PermissionManager.checkIsStoragePermissionGranted(context!!)) createGalleryIntent()
        else PermissionRequester.requestStoragePermission(this)
    }

    private fun createGalleryIntent() {
        val intent = Intent(Intent.ACTION_PICK).apply { type = "image/*" }
        startActivityForResult(intent, RC_SELECT_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            RC_SELECT_IMAGE -> if (resultCode == Activity.RESULT_OK) {
                val selectedImage = FileUtil.getImageFromUri(context!!, data!!.data!!)
                if (FileUtil.isSizeValid(selectedImage)) {
                    model.setImageToEvent(selectedImage)
                    model.imagePath = selectedImage.path
                } else {
                    toast(getString(R.string.file_too_large))
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            RC_PERMISSION_STORAGE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    createGalleryIntent()
            }
        }
    }
}