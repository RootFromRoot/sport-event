package com.app.sportevent.ui.list.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.sportevent.R
import com.app.sportevent.data.BASE_URL
import com.app.sportevent.data.ENDPOINT_ICON
import com.app.sportevent.data.ENDPOINT_IMAGES
import com.app.sportevent.data.poko.Event
import com.app.sportevent.data.util.DateUtil
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_event.view.*
import timber.log.Timber

class EventListAdapter : RecyclerView.Adapter<EventHolder>() {

    private var items: ArrayList<Event> = ArrayList()
    var onItemClick: (id: Int) -> Unit = {}

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = EventHolder(
        LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_event, parent, false)
    )

    fun setItems(items: ArrayList<Event>) {
        this.items = items
        notifyDataSetChanged()
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: EventHolder, position: Int) {
        val item = items[position]
        if (item.id == 13) {
            Timber.i("id == 13")
        }
        holder.itemView.apply {
            this.setOnClickListener { onItemClick(item.id) }
            tv_user_name.text = item.user.name
            tv_name.text = item.name
            tv_description.text = item.description
            tv_age.text = "${item.age_min} - ${item.age_max}"
            tv_location.text = "${item.city?.name}, ${item.country?.name}"
            tv_free_place.text = "${item.participant_count}"
            tv_member_type.text = item.memberType?.name
            tv_age.text = "${item.age_min} - ${item.age_max}"
            if (item.date_start != null && item.date_stop != null) {
                DateUtil.apply {
                    val m = item.date_start!! * 1000
                    val mEnd = item.date_stop!! * 1000
                    tv_start_date.text =
                        "${millisToFormattedDay(m)}.${millisToFormattedMonth(m)}.${millisToYear(m)} -"
                    tv_end_date.text =
                        "${millisToFormattedDay(mEnd)}.${millisToFormattedMonth(mEnd)}.${
                            millisToYear(mEnd)
                        }"
                }
            }
            val url = BASE_URL + ENDPOINT_ICON + item.sportKind?.icon
            Picasso
                .get()
                .load(url)
                .into(iv_sport_icon, object : Callback {
                    override fun onError(e: Exception?) {
                        Timber.e(e, "Failed to load image")
                        e?.printStackTrace()
                    }

                    override fun onSuccess() {
                        Timber.i("")
                    }
                })
            if (item.icon != null) {
                val imageUrl = BASE_URL + ENDPOINT_IMAGES + item.icon
                Picasso
                    .get()
                    .load(imageUrl)
                    .resize(0, 400)
                    .into(iv_image, object : Callback {
                        override fun onError(e: Exception?) {
                            Timber.e(e, "Failed to load image")
                        }

                        override fun onSuccess() {
                            Timber.i("")
                        }
                    })
            } else {
                iv_image.visibility = View.GONE
            }
        }
    }


}

class EventHolder(view: View) : RecyclerView.ViewHolder(view)