package com.app.sportevent.ui.filter.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.sportevent.R
import com.app.sportevent.data.poko.*
import com.app.sportevent.data.toast
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.data.util.DateUtil
import com.app.sportevent.data.util.DialogBuilder
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.filter.FilterModel
import com.app.sportevent.ui.filter.ui.city.CityFilterFragment
import com.app.sportevent.ui.filter.ui.country.CountryFilterFragment
import com.app.sportevent.ui.filter.ui.date.RangeDateFragment
import com.app.sportevent.ui.filter.ui.kind.SportFilterFragment
import com.app.sportevent.ui.filter.ui.type.event.EventTypeFragment
import com.app.sportevent.ui.filter.ui.type.member.MemberFilterFragment
import kotlinx.android.synthetic.main.fragment_filter.*
import org.koin.android.ext.android.inject

class FilterEventsFragment : BaseFragment() {

    private val model: FilterModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_filter, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        model.initFilterData { }
        setupUi()
    }

    override fun onResume() {
        super.onResume()
        initViews()
    }

    private fun initViews() {
        setAgeRangeToView(model.ageMin, model.ageMax)
        setSportKindsToView(model.sportKindList)
        setEventTypesToView(model.eventTypeList)
        setMemberTypesToView(model.memberTypeList)
        setCountriesToView(model.countryList)
        setCitiesToView(model.cityList)
        setDatesToView(model.startDate, model.endDate)
    }

    private fun setDatesToView(mS: Long, mE: Long) {
        if (mS != 0L) DateUtil.apply {
            val startTxt =
                "${millisToFormattedDay(mS)}.${millisToFormattedMonth(mS)}.${millisToYear(mS)}"
            val endTxt =
                "${millisToFormattedDay(mE)}.${millisToFormattedMonth(mE)}.${millisToYear(mE)}"
            tv_date.text = "$startTxt - $endTxt"
        } else tv_date.text = ""
    }

    private fun setCitiesToView(list: ArrayList<City>) {
        val selectedList = list.filter { it.isSelected }
        if (selectedList.size == 1) tv_city.text = selectedList[0].name
        else {
            var text = ""
            selectedList.forEachIndexed { i, it ->
                if (it.isSelected) {
                    val title = if (i != selectedList.size - 1)
                        "${it.name}," else
                        it.name
                    text = "$text $title"
                }
            }
            tv_city.text = text
        }
    }

    private fun setCountriesToView(list: ArrayList<CountryModel>) {
        val selectedList = list.filter { it.isSelected }
        if (selectedList.size == 1) tv_country.text = selectedList[0].name
        else {
            var text = ""
            selectedList.forEachIndexed { i, it ->
                if (it.isSelected) {
                    val title = if (i != selectedList.size - 1)
                        "${it.name}," else
                        it.name
                    text = "$text $title"
                }
            }
            tv_country.text = text
        }
    }

    private fun setMemberTypesToView(list: ArrayList<TypeParticipantModel>) {
        val selectedList = list.filter { it.isSelected }
        if (selectedList.size == 1) tv_member_type.text = selectedList[0].name
        else {
            var text = ""
            selectedList.forEachIndexed { i, it ->
                if (it.isSelected) {
                    val title = if (i != selectedList.size - 1)
                        "${it.name}," else
                        it.name
                    text = "$text $title"
                }
            }
            tv_member_type.text = text
        }
    }

    private fun setEventTypesToView(list: ArrayList<TypeEventModel>) {
        val selectedList = list.filter { it.isSelected }
        if (selectedList.size == 1) tv_event_type.text = selectedList[0].name
        else {
            var text = ""
            selectedList.forEachIndexed { i, it ->
                if (it.isSelected) {
                    val title = if (i != selectedList.size - 1)
                        "${it.name}," else
                        it.name
                    text = "$text $title"
                }
            }
            tv_event_type.text = text
        }
    }

    private fun setSportKindsToView(list: ArrayList<SportKindModel>) {
        val selectedList = list.filter { it.isSelected }
        if (selectedList.size == 1) tv_sport_kind.text = selectedList[0].name
        else {
            var text = ""
            selectedList.forEachIndexed { i, it ->
                if (it.isSelected) {
                    val title = if (i != selectedList.size - 1) "${it.name}," else it.name
                    text = "$text $title"
                }
            }
            tv_sport_kind.text = text
        }
    }

    private fun setAgeRangeToView(ageMin: Int, ageMax: Int) {
        if (ageMin != 0 && ageMax != 0) {
            tv_age.text = "$ageMin - $ageMax"
        }
    }

    private fun setupUi() {
        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        wrp_age.setOnClickListener {
            DialogBuilder.showAgeDialog(context!!, model.ageMin, model.ageMax) { min, max ->
                setAgeRangeToView(min, max)
                model.ageMin = min
                model.ageMax = max
                model.filter {}
            }
        }
        wrp_sport_kind.setOnClickListener {
            AppFragmentManager.openFragment(activity!!, SportFilterFragment())
        }
        wrp_event_type.setOnClickListener {
            AppFragmentManager.openFragment(activity!!, EventTypeFragment())
        }
        wrp_member_type.setOnClickListener {
            AppFragmentManager.openFragment(activity!!, MemberFilterFragment())
        }
        wrp_country.setOnClickListener {
            AppFragmentManager.openFragment(activity!!, CountryFilterFragment())
        }
        wrp_city.setOnClickListener {
            if (model.countryList.isNotEmpty())
                AppFragmentManager.openFragment(activity!!, CityFilterFragment())
            else toast("Сначала выберите страну")
        }
        wrp_date.setOnClickListener {
            AppFragmentManager.openFragment(activity!!, RangeDateFragment())
        }

        tv_reset.setOnClickListener {
            model.resetFilter()
            initViews()
        }
    }
}