package com.app.sportevent.ui.event.create.ui.filter


import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.sportevent.R
import com.app.sportevent.data.poko.SportKindModel
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.data.util.AppTextWatcher
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.create.CreateEventModel
import com.app.sportevent.ui.event.create.ui.SportKindAdapter
import com.app.sportevent.ui.filter.ui.kind.SportKindListAdapter
import kotlinx.android.synthetic.main.fragment_filter_base.*
import org.koin.android.ext.android.inject

class SportKindFragment : BaseFragment() {

    private val model: CreateEventModel by inject()
    private val adapter = SportKindAdapter()
    private var sportKindList: ArrayList<SportKindModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_filter_base, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
        sportKindList = model.getFilterData().sportKind
        sportKindList.find { it.id == model.selectedSportKind.id }?.isSelected = true
        adapter.setItems(sportKindList)
    }

    private fun setupUi() {
        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        tv_toolbar_title.text = "Виды спорта"

        rv_sport_list.layoutManager = LinearLayoutManager(activity!!)
        rv_sport_list.adapter = adapter

        btn_reset_filter.setOnClickListener {
            sportKindList.forEach { it.isSelected = false }
            model.selectedSportKind = SportKindModel()
            adapter.setItems(sportKindList)
        }

        et_search.addTextChangedListener(object : AppTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null) adapter.filter.filter(s)
            }
        })

        adapter.onItemCheck = { kind ->
        //    sportKindList.find { it.id == kind.id }?.isSelected = true
//            sportKindList.forEach { it.isSelected = it.id == kind.id }
            //rv_sport_list.post { adapter.setItems(sportKindList) }
            model.selectedSportKind = kind
        }
    }
}