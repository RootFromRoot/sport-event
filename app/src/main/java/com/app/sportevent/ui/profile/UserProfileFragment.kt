package com.app.sportevent.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.sportevent.R
import com.app.sportevent.data.OPENED_FROM_MY_PROFILE
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.ui.MainActivity
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.create.ui.CreateEventFirstFragment
import kotlinx.android.synthetic.main.fragment_my_profile.*

class UserProfileFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_my_profile, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
    }

    private fun setupUi() {
        createBtn.setOnClickListener {
            activity().openedFrom = OPENED_FROM_MY_PROFILE
            activity().onBackClick = {}
            AppFragmentManager.openFragment(activity!!,
                CreateEventFirstFragment()
            )
        }
    }
}