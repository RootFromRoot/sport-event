package com.app.sportevent.ui.filter

import com.app.sportevent.data.Api
import com.app.sportevent.data.LANG_RU
import com.app.sportevent.data.NO_CONNECTION_TEXT
import com.app.sportevent.data.SERVER_RESPONSE_OK
import com.app.sportevent.data.builder.DictionariesBuilder
import com.app.sportevent.data.poko.*
import com.app.sportevent.data.util.PrefManager
import com.prolificinteractive.materialcalendarview.CalendarDay
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.ConnectException

class FilterModel(private val prefManager: PrefManager) {

    private val api = Api.get()
    var onError: (message: String?) -> Unit = {}
    var filterData: DictionariesResponse = DictionariesBuilder.empty()

    var sportKindList: ArrayList<SportKindModel> = ArrayList()
    var eventTypeList: ArrayList<TypeEventModel> = ArrayList()
    var memberTypeList: ArrayList<TypeParticipantModel> = ArrayList()
    var ageMax = 60
    var ageMin = 4

    var countryList: ArrayList<CountryModel> = ArrayList()
    var cityList: ArrayList<City> = ArrayList()
    var selectedDates: List<CalendarDay> = listOf()
    var startDate = 0L
    var endDate = 0L

    fun filter(onSuccess: () -> Unit) {
        GlobalScope.launch {
            try {
                val request = Api.get().filer(
                    token = prefManager.getToken(),
                    sportKindIdList = mapOf(
                        Pair("sport_kind", sportKindList.filter { it.isSelected }.map { it.id })
                    ),
                    typeEventIdList = mapOf(
                        Pair("type_event", eventTypeList.filter { it.isSelected }.map { it.id })
                    ),
                    memberTypeIdList = mapOf(
                        Pair(
                            "type_participant",
                            memberTypeList.filter { it.isSelected }.map { it.id })
                    ),
                    ageMinMaxList = mapOf(Pair("age", listOf(ageMin, ageMax))),
                    countryIdList = mapOf(
                        Pair("country", countryList.filter { it.isSelected }.map { it.id })
                    ),
                    cityIdList = mapOf(
                        Pair(
                            "city",
                            cityList.filter { it.isSelected }.map { it.id })
                    ),
                    dateStartEndList = mapOf(Pair("date", listOf(startDate, endDate)))
                ).await()
                if (request.isSuccessful) {
                    if (request.body()!!.status == SERVER_RESPONSE_OK) onSuccess()
                }
            } catch (e: ConnectException) {
                onError(NO_CONNECTION_TEXT)
            }
        }
    }

    fun initFilterData(onFetched: (dictionaries: DictionariesResponse) -> Unit) {
        GlobalScope.launch {
            val response = api.getCommonData(prefManager.getToken(), LANG_RU).await()
            if (response.isSuccessful) {
                if (response.body()!!.status == SERVER_RESPONSE_OK)
                    withContext(Dispatchers.Main) {
                        filterData = response.body()!!
                        onFetched(response.body()!!)
                    }
            }
        }
    }

    fun getCityListInCounty() {
        val selectedCountryList = countryList.filter { it.isSelected }
        selectedCountryList.forEach { country ->
            GlobalScope.launch {
                try {
                    val response = api.getCityListByCountry(
                        lang = LANG_RU,
                        word = "",
                        isoList = arrayListOf(country.iso)
                    ).await()
                    if (response.isSuccessful) {
                        if (response.body()!!.status == SERVER_RESPONSE_OK)
                            withContext(Dispatchers.Main) {
                                cityList.addAll(response.body()!!.cities)
                            }
                    }
                } catch (e: ConnectException) {
                    onError(NO_CONNECTION_TEXT)
                }
            }
        }
    }

    fun resetFilter() {
        sportKindList = ArrayList()
        eventTypeList = ArrayList()
        memberTypeList = ArrayList()
        countryList = ArrayList()
        cityList = ArrayList()
        ageMax = 60
        ageMin = 4
        startDate = 0L
        endDate = 0L
        filter {  }
    }
}