package com.app.sportevent.ui.event.create.ui.preview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.sportevent.R
import com.app.sportevent.data.*
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.ui.MainActivity
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.create.CreateEventModel
import com.app.sportevent.ui.list.ui.EventListFragment
import com.app.sportevent.ui.profile.UserProfileFragment
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_event_preview.*
import kotlinx.android.synthetic.main.item_sport_filter.view.*
import org.koin.android.ext.android.inject
import timber.log.Timber
import java.lang.Exception

class CreatingEventPreviewFragment : BaseFragment() {

    private val model: CreateEventModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_event_preview, container, false)!!

    override fun onResume() {
        super.onResume()
        initView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
    }

    private fun setupUi() {
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        btn_edit.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        btn_create.setOnClickListener {
            model.createEvent {
                if (it) {
                    toast("Ивент создан")
                    AppFragmentManager.clearBackStack(activity!!)
                    val fragment = when ((activity as MainActivity).openedFrom) {
                        OPENED_FROM_MY_PROFILE -> {
                            (activity as MainActivity).setSelectedMyProfileFragment()
                            UserProfileFragment()
                        }
                        OPENED_FROM_EVENT_LIST -> {
                            (activity as MainActivity).setSelectedListFragment()
                            EventListFragment()
                        }
                        else -> {
                            (activity as MainActivity).setSelectedListFragment()
                            EventListFragment()
                        }
                    }
                    AppFragmentManager.openFragment(activity!!, fragment)
                }
                else toast("Ошибка. Заполните все поля!")
            }
        }
    }

    private fun initView() {
        toolbarTitle.text = if (model.title.isNotEmpty()) model.title else "Создание ивента"
        view_pager.adapter = CreateEventPagerAdapter(activity!!.supportFragmentManager)
        tab_layout.setupWithViewPager(view_pager)

        /*val url = BASE_IC_URL + ENDPOINT_ICON + model.selectedSportKind.icon
        Picasso.get().load(url).into(iv_sport_kind,object : Callback {
            override fun onError(e: Exception?) {
                e?.printStackTrace()
            }

            override fun onSuccess() {
                Timber.i("")
            }
        })*/
    }
}

