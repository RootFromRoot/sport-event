package com.app.sportevent.ui.event.edit.ui.preview

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.app.sportevent.ui.event.edit.ui.preview.page.EditEventPreviewFeedbackFragment
import com.app.sportevent.ui.event.edit.ui.preview.page.EditEventPreviewFinanceFragment
import com.app.sportevent.ui.event.edit.ui.preview.page.EditEventPreviewGeneralFragment
import com.app.sportevent.ui.event.edit.ui.preview.page.EditEventPreviewMembersFragment

class EditEventPagerAdapter(fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount() = 4

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> EditEventPreviewGeneralFragment()
            1 -> EditEventPreviewMembersFragment()
            2 -> EditEventPreviewFinanceFragment()
            3 -> EditEventPreviewFeedbackFragment()
            else -> EditEventPreviewGeneralFragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Общее"
            1 -> "Участники"
            2 -> "Финансы"
            3 -> "Отзывы"
            else -> "Общее"
        }
    }
}