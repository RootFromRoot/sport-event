package com.app.sportevent.ui.event.create.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.sportevent.R
import com.app.sportevent.data.isNumber
import com.app.sportevent.data.util.AppFragmentManager
import com.app.sportevent.data.util.AppTextWatcher
import com.app.sportevent.data.util.TextInputUtil
import com.app.sportevent.ui.base.BaseFragment
import com.app.sportevent.ui.event.create.CreateEventModel
import com.app.sportevent.ui.event.create.ui.preview.CreatingEventPreviewFragment
import kotlinx.android.synthetic.main.fragment_create_event_third.*
import org.koin.android.ext.android.inject

class CreateEventThirdFragment : BaseFragment() {

    private val model: CreateEventModel by inject()
    private var isValid = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_create_event_third, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUi()
    }

    override fun onResume() {
        super.onResume()
        setMemberCountToView(model.memberCount)
        setIncludedToPayment(model.includedToPayment)
        setMemberPaymentToView(model.memberPayment)
        setPrizePoolToView(model.prizePool)
    }

    private fun setIncludedToPayment(includedToPayment: String) {
        if (includedToPayment.isNotEmpty()) et_included_to_payment.setText(includedToPayment)
    }

    private fun setPrizePoolToView(prizePool: String) {
        if (prizePool.isNotEmpty()) et_prize_pool.setText(prizePool)
    }

    private fun setMemberPaymentToView(memberPayment: String) {
        if (memberPayment.isNotEmpty()) et_member_payment.setText(memberPayment)
    }

    private fun setMemberCountToView(memberCount: Int) {
        if (memberCount != 0) et_member_count.setText(memberCount.toString())
    }

    private fun setupUi() {
        iv_back.setOnClickListener { AppFragmentManager.closeFragment(activity!!) }
        activity().onBackClick = { AppFragmentManager.closeFragment(activity!!) }
        nextBtn.setOnClickListener {
            AppFragmentManager.openFragment(activity!!, CreatingEventPreviewFragment())
        }

        et_member_count.addTextChangedListener(object : AppTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty() || !s.toString().isNumber()) {
                    isValid = false
                    til_member_count.error = "Должно быть числом"
                } else {
                    model.memberCount = s.toString().trim().toInt()
                    til_member_count.error = ""
                }
            }
        })

        et_member_payment.addTextChangedListener(object : AppTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                model.memberPayment = s.toString().trim()
            }
        })

        et_included_to_payment.addTextChangedListener(object : AppTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                model.includedToPayment = s.toString().trim()
            }
        })

        et_prize_pool.addTextChangedListener(object : AppTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                model.prizePool = s.toString().trim()
            }
        })

        TextInputUtil.setObservedEtEnteredLenghtToTV(et_prize_pool, tv_prize_pool_lenght)
        TextInputUtil.setObservedEtEnteredLenghtToTV(et_member_payment, tv_member_payment_lenght)
        TextInputUtil.setObservedEtEnteredLenghtToTV(
            et_included_to_payment, tv_included_to_payment_lenght
        )
    }
}